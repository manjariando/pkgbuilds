# Maintainer: Denis A. Altoé Falqueto <denisfalqueto@gmail.com>
# Contributor: Geyslan G. Bem <geyslan@gmail.com>

pkgname=safesignidentityclient
_library=3.5.4112
pkgver=3.5.4148
pkgrel=4
pkgdesc="Smart card PKCS#11 provider and token manager"
arch=('x86_64')
url="https://pronova.com.br/download-tokens"
license=('custom:copyright')
depends=('pcsclite' 'gdbm183' 'wxgtk2' 'openssl-1.0' 'xdg-utils' 'libmspack' 'ccid')
optdepends=('acsccid: ACS CCID PC/SC driver'
            'scmccid: binary driver for the SCM Smart Card Readers'
            'sac-core: Safenet Authentication Client'
            'opensc-openct: drivers for several smart card readers')
install=${pkgname}.install
source_x86_64=("http://www.pronova-usa.com/downloads/aet/SafeSign_3_5_Ubuntu_1604LTS_x86_x64.zip"
            "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
            "https://metainfo.manjariando.com.br/${pkgname}/tokenutils"-{64,128}.png
            "pkcs11.conf")
sha512sums_x86_64=('d56bb7f8918a9d488597389e95f032181cb9e356e245406a672fe8fc899153f664bc6ec620b0d166c2340ff2c5a7259c837e0ce762c6a615da6e253e7f461fe0'
                   '1ebcf3dbc16158d64de482315d2c8d989e4098292791471dd907a078253418ddee3be0dba678ca5c93bc49fbeae2af03f5451c3491be2ce7053dd04846e6d046'
                   'b9e451e3f3d19af19f7e9d8281a5653471242d7d317efc59d19ddca7c6c866d2171730ac314ea157545f93cf02779e341b2e1c855b905cbcfe3749951d3cf3b2'
                   'f13800ba52c3c2a508c7df71d60d3596a649d9c92e7e8117d03309c1b4418005c3219cc06fdec3b9a0e12341465d35d7c3daa23de3cf88c9edef1c1612b65e48'
                   'f72ca7cf37f315a64234e88f481d989a07948a0b0e4f49de251b4769e009f60fd8cb7f32eb94ebd95f2b61549b6e771fa0952d59375f4aa946ffb8324d37c259')

prepare() {
    ar x SafeSign_IC_Standard_Linux_3.5.0.0_Ubuntu_1604LTS_x86_64.deb
    tar xvf data.tar.xz
}

package() {
    cp -R ${srcdir}/usr ${pkgdir}
    install -D -m644 "${srcdir}/pkcs11.conf" "${pkgdir}/usr/share/safesign/pkcs11.conf"
    sed -i "s:LIBRARY=.*:LIBRARY=${_library}:" "${startdir}/${pkgname}.install"

    # Appstream
    install -Dm644 "${srcdir}/usr/share/doc/${pkgname}/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/copyright"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/tokenadmin.desktop" "${pkgdir}/usr/share/applications/com.tokenadmin.desktop"

    for i in 64 128; do
        install -Dm644 "${srcdir}/tokenutils-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/tokenutils.png"
    done
}
