# Maintainer: tioguda <tioguda@gmail.com>

pkgname=safenetauthenticationclient
_pkgver=10.0.37-0
pkgver=${_pkgver/-/.}
pkgrel=4
pkgdesc="SafeNet Authentication Client Installation package . Install this package if you want eToken middleware running on your system."
arch=('i686' 'x86_64')
url="https://safenet.gemalto.com"
license=('custom')
groups=('')
depends=('atk' 'cairo' 'fontconfig' 'freetype2' 'gdk-pixbuf2' 'glib2' 'gtk2' 'kmod' 'libx11' 'openssl' 'pango' 'pcsc-tools' 'pcsclite' 'openct')
optdepends=('opensc-openct: drivers for several smart card readers'
        'scmccid: driver for the SCM smart card readers')
conflicts=('sac-core')
backup=('etc/eToken.common.conf' 'etc/eToken.conf')
options=('!strip' '!emptydirs')
install=${pkgname}.install
source=("https://metainfo.manjariando.com.br/${pkgname}/"com.{SACMonitor,SACTools}.metainfo.xml)
source_i686=("https://manjariando.gitlab.io/source-packages/${pkgname}/SafenetAuthenticationClient-BR-${_pkgver}_i386.deb")
source_x86_64=("https://manjariando.gitlab.io/source-packages/${pkgname}/SafenetAuthenticationClient-BR-${_pkgver}_amd64.deb")
sha512sums=('856de4b3d59c8415d78503dc098debf6527a22cdec756c52cdc503ca34871de3bd0f12447b6b6a75a139476b422af8e895d83dba0c436df90556b436c46eae46'
            '1ee67a6dacc8d26cbcd585f439c5aa93318e9d99f37907aa0d6a163c52a947993f5020a16444aa0f8835d8804728c3bddc8776728cc48f739c1e03ca618e33fe')
sha512sums_i686=('61bb39d971d013df1e423e00c756f87211c5b4242785a0dfeb0dacd29146c938ac751e578f37396b1feca956d92668babee3e3c07099c613a14b210ddce9c68f')
sha512sums_x86_64=('85c583be5f23a48d09a3eb2c094041fa2ce8531a9cdb72828f178ddac7b9be618888b1a9fbc4b7516063aac6891ca526c848eadbb4cf88abcee3710b4cf6ff93')

package(){

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    # Fix directories structure differencies
    mkdir -p "${pkgdir}"/usr/lib
    mv "${pkgdir}"/lib/* "${pkgdir}"/usr/lib
    rm -rf "${pkgdir}"/lib "${pkgdir}"/usr/lib64
    rm -rf "${pkgdir}"/etc/{init.d,rc.d}

    # Appstream
    install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/SACMonitor.desktop" "${pkgdir}/usr/share/applications/com.SACMonitor.desktop"
    install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/SACTools.desktop" "${pkgdir}/usr/share/applications/com.SACTools.desktop"
    install -Dm644 "${srcdir}/com.SACMonitor.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.SACMonitor.metainfo.xml"
    install -Dm644 "${srcdir}/com.SACTools.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.SACTools.metainfo.xml"
    install -Dm644 "${pkgdir}/usr/share/doc/eToken/LICENSE.txt" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    for i in 16 48 64 72 128 256; do
        install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/icons/Applc-RTE-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/SACMonitor.png"
        install -Dm644 "${pkgdir}/usr/share/eToken/shortcuts/icons/Applc-RTE-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/SACTools.png"
    done

    install -Dm644 -t "${pkgdir}/etc/xdg/menus/applications-merged" "${pkgdir}/usr/share/eToken/shortcuts/SafeNet.menu"
    install -d "${pkgdir}/usr/share/desktop-directories"
    cp -r "${pkgdir}"/usr/share/eToken/shortcuts/*.directory "${pkgdir}/usr/share/desktop-directories"

    # Archify folder permissions
    cd ${pkgdir}
    for d in $(find . -type d); do
        chmod 755 $d
    done

}
