# Maintainer: tioguda <guda.flavio@gmail.com>

pkgbase=droidcam
kernel=5.10
_extramodules=extramodules-${kernel}-MANJARO
_linuxprefix=linux${kernel/./}
pkgname=${_linuxprefix}-${pkgbase}
_pkgver=1.7
pkgver=1.7_5.10.26_1
pkgrel=1
pkgdesc='Driver for Droidcam'
arch=('x86_64')
url="https://github.com/aramg/${pkgbase}"
license=('GPL')
makedepends=("${_linuxprefix}" "${_linuxprefix}-headers")
provides=("${pkgbase}-modules=${kernel}" "${_linuxprefix}-${pkgbase}")
conflicts=("${pkgbase}-dkms" "${pkgbase}-dkms-git")
groups=("$_linuxprefix-extramodules")
install="${pkgbase}.install"
source=("${pkgbase}-${_pkgver}.zip::${url}/archive/v${_pkgver}.zip")
sha512sums=('eaf06b2c51a888da752819d765055da05fdb8997202825809fd88b9ea7e7ffbbac9ba1ee9bbc274e075bc7cd0d50d5e233bd126ecbabd95c93e9ebe62d498d2a')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    cd "${srcdir}/${pkgbase}-${_pkgver}"
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${srcdir}/${pkgbase}-${_pkgver}"

    make -C /usr/lib/modules/$_kernver/build \
    M="${srcdir}/${pkgbase}-${_pkgver}/v4l2loopback" \
    modules
}

package() {
    depends=("${_linuxprefix}")
    cd "${srcdir}/${pkgbase}-${_pkgver}"

    install -Dm755 "v4l2loopback/v4l2loopback-dc.ko" "${pkgdir}/usr/lib/modules/${_extramodules}/v4l2loopback-dc.ko"
    find "${pkgdir}" -name '*.ko' -exec xz {} +

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${pkgbase}.install"
}

