# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Contributor: Maxime Gauduin <alucryd@gmail.com>
# Contributor: mortzu <me@mortzu.de>
# Contributor: fnord0 <fnord0@riseup.net>

_linuxprefix=linux510-xanmod
_extramodules=extramodules-5.10-xanmod-MANJARO
pkgname=$_linuxprefix-acpi_call
_pkgname=acpi_call
_pkgver=1.1.0
pkgver=1.1.0_5.10.27.xanmod1_1.1
pkgrel=1
pkgdesc='A linux kernel module that enables calls to ACPI methods through /proc/acpi/call'
arch=('x86_64')
url="http://github.com/mkottman/acpi_call"
license=('GPL')
depends=("$_linuxprefix")
makedepends=("$_linuxprefix-headers")
provides=("$_pkgname")
groups=("$_linuxprefix-extramodules")
install=$_pkgname.install
source=("$pkgname-$_pkgver.tar.gz::${url}/archive/v${_pkgver}.tar.gz"
        'kernel56.patch')
sha256sums=('d0d14b42944282724fca76f57d598eed794ef97448f387d1c489d85ad813f2f0'
            'a31f35f848abbf9a3b4e8eeb61bcda4ad814565f81fc686a2518948941e45641')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    _kernel=${_ver}
    printf '%s' "${_pkgver}_${_kernel/-/_}"
}

prepare() {
  sed -i -e 's|acpi/acpi.h|linux/acpi.h|g' acpi_call-${_pkgver}/acpi_call.c
  sed -i -e 's|asm/uaccess.h|linux/uaccess.h|g' acpi_call-${_pkgver}/acpi_call.c
  cd "${_pkgname}-${_pkgver}"
  patch -p1 -i ../kernel56.patch
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  cd "${_pkgname}-${_pkgver}"

  make KVERSION="${_kernver}"
}

package() {
  cd "${_pkgname}-${_pkgver}"

  install -dm 755 "${pkgdir}"/usr/lib/{modules/${_extramodules},modules-load.d}
  install -m 644 acpi_call.ko "${pkgdir}"/usr/lib/modules/${_extramodules}/
  gzip "${pkgdir}"/usr/lib/modules/${_extramodules}/acpi_call.ko
  echo acpi_call > "${pkgdir}"/usr/lib/modules-load.d/${pkgname}.conf

  install -dm 755 "${pkgdir}"/usr/share/${pkgname}
  cp -dr --no-preserve='ownership' {examples,support} "${pkgdir}"/usr/share/${pkgname}/

  sed -i "s/EXTRAMODULES=.*/EXTRAMODULES=$_extramodules/" \
    "$startdir/acpi_call.install"
}
