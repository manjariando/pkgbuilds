# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

_linuxprefix=linux54-xanmod
_extramodules=extramodules-5.4-xanmod-MANJARO
pkgname=$_linuxprefix-nvidiabl
_pkgname=nvidiabl
_pkgver=0.88
pkgver=0.88_5.4.108.xanmod1_1.1
pkgrel=1
epoch=
pkgdesc="Driver to adjust display backlight on modern mobile NVidia graphics adapters."
arch=('x86_64')
url="https://github.com/guillaumezin/$_pkgname"
license=('GPL')
depends=()
makedepends=($_linuxprefix{,-headers})
optdepends=()
conflicts=(nvidia-bl)
provides=("$_pkgname=$_pkgver")
groups=("$_linuxprefix-extramodules")
install=$_pkgname.install
source=("$_pkgname-$_pkgver.zip::$url/archive/master.zip" 
        "nvidiabl-master.patch"
        "kernel-4.8.patch")
md5sums=('c1cec85b0a9f4469d433a2756bdb2497'
         '79e5699da970908d6ec8dba114df87cb'
         'd94abebc5f40586fbed015d9fab29a37')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
  cd "${srcdir}/nvidiabl-master/"
  patch -p1 -i "${srcdir}/nvidiabl-master.patch"
  patch -p1 -i "${srcdir}/kernel-4.8.patch"
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  cd "${srcdir}/nvidiabl-master/"
  make KVER="${_kernver}"
}

package() {
  depends=("${_linuxprefix}")

  cd "${srcdir}/nvidiabl-master/"
  MODPATH="${pkgdir}/usr/lib/modules/${_extramodules}/"
  install -d $MODPATH
  install -m 644 -c nvidiabl.ko $MODPATH
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidiabl.install"
  sed -i -e "s/VERSION='.*'/VERSION='${_linuxprefix}'/" "${startdir}/nvidiabl.install"
}
