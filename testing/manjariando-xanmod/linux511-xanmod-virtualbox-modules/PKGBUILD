# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Ionut Biru <ibiru@archlinux.org>
# Sébastien Luttringer <seblu@aur.archlinux.org>

_linuxprefix=linux511-xanmod
_extramodules=extramodules-5.11-xanmod-MANJARO
pkgbase=$_linuxprefix-virtualbox-modules
pkgname=("$_linuxprefix-virtualbox-host-modules" "$_linuxprefix-virtualbox-guest-modules")
_pkgver=6.1.18
pkgver=6.1.18_5.11.11.xanmod1_1.1
pkgrel=1
arch=('x86_64')
url='http://virtualbox.org'
license=('GPL')
depends=("$_linuxprefix")
groups=("$_linuxprefix-extramodules")
makedepends=("virtualbox-host-dkms>=$_pkgver"
             "virtualbox-guest-dkms>=$_pkgver"
             'dkms'
             "$_linuxprefix" "$_linuxprefix-headers"
             'binutils<=2.35.2')
#source=("fixes_5.11.patch")
#sha256sums=('19c78b00215d17fd1b05cd62f18683964a998cf67fd2323932d0cfa1afd02b89')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
  _kernver="$(cat /usr/lib/modules/$_extramodules/version)"
  # dkms need modification to be run as user
  cp -r /var/lib/dkms .
  echo "dkms_tree='$srcdir/dkms'" > dkms.conf
 
  rm "$srcdir/dkms/vboxhost/${_pkgver}_OSE/source"
  cp -r "/usr/src/vboxhost-${_pkgver}_OSE/" "$srcdir/dkms/vboxhost/${_pkgver}_OSE/"
  mv "$srcdir/dkms/vboxhost/${_pkgver}_OSE/vboxhost-${_pkgver}_OSE/" "$srcdir/dkms/vboxhost/${_pkgver}_OSE/source/"
  cd "$srcdir/dkms/vboxhost/${_pkgver}_OSE/source/"

#  patch -p1 -i "$srcdir/fixes_5.11.patch"  
}

build() {
  _kernver="$(cat /usr/lib/modules/$_extramodules/version)"
  # build host modules
  msg2 'Host modules'
  dkms --dkmsframework dkms.conf build "vboxhost/${_pkgver}_OSE" -k "$_kernver"
}

package_linux511-xanmod-virtualbox-host-modules(){
  pkgdesc='Host kernel modules for VirtualBox'
  replaces=("$_linuxprefix-virtualbox-modules")
  conflicts=("$_linuxprefix-virtualbox-modules")
  provides=('VIRTUALBOX-HOST-MODULES')
  install=virtualbox-host-modules.install

  _kernver="$(cat /usr/lib/modules/$_extramodules/version)"

  install -dm755 "$pkgdir/usr/lib/modules/$_extramodules"
  cd "dkms/vboxhost/${_pkgver}_OSE/$_kernver/$CARCH/module"
  install -m644 * "$pkgdir/usr/lib/modules/$_extramodules"
  find "$pkgdir" -name '*.ko' -exec gzip -9 {} +
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='$_extramodules'/" "$startdir/virtualbox-host-modules.install"

  mkdir -p "$pkgdir/etc/modules-load.d"
  echo "vboxdrv" > "$pkgdir/etc/modules-load.d/$_linuxprefix-virtualbox-host-modules.conf"
  echo "vboxnetadp" >> "$pkgdir/etc/modules-load.d/$_linuxprefix-virtualbox-host-modules.conf"
  echo "vboxnetflt" >> "$pkgdir/etc/modules-load.d/$_linuxprefix-virtualbox-host-modules.conf"
}

package_linux511-xanmod-virtualbox-guest-modules(){
  pkgdesc='Guest kernel modules for VirtualBox'
  replaces=("$_linuxprefix-virtualbox-manjaro-modules")
  conflicts=("$_linuxprefix-virtualbox-manjaro-modules")
  provides=('VIRTUALBOX-GUEST-MODULES')

  echo "Guest-Modules are now built-in"
}

