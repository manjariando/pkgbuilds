# Maintainer: Rafał Kozdrój <kozeid2@gmail.com>
# Contributor: kikadf <kikadf.01@gmail.com>
# Contributor: Daniel Henry <d at hackr dot pl>
# Contributor: Miguel Revilla <yo at  miguelrevilla dot com>
# Contributor: Alfonso Saavedra "Son Link" <sonlink.dourden@gmail.com>
# Contributor: Hexchain Tong <i at hexchain dot org>

pkgname=megasync
pkgver=4.4.0.0
pkgrel=1
pkgdesc="Easy automated syncing between your computers and your MEGA cloud drive"
arch=('i686' 'x86_64')
url="https://github.com/meganz/MEGAsync"
license=('custom:MEGA LIMITED CODE REVIEW LICENCE')
depends=('c-ares' 'crypto++' 'libsodium' 'hicolor-icon-theme' 'libuv'
         'qt5-svg' 'qt5-x11extras' 'libmediainfo' 'libraw' 'qt5-base' 'ffmpeg' 'libpdfium')
makedepends=('qt5-tools' 'swig' 'doxygen' 'lsb-release' 'git')
_extname="_Linux"
#_sdk_commit="3cc68f5aea1148472ca200ebac2079850e090de2"
source=("git+https://github.com/meganz/MEGAsync.git#tag=v${pkgver}${_extname}"
        "meganz-sdk::git+https://github.com/meganz/sdk.git" #commit=${_sdk_commit}"
        "https://manjariando.gitlab.io/metainfo/${pkgname}/com.${pkgname}.metainfo.xml"
        "pdfium.patch::https://aur.archlinux.org/cgit/aur.git/plain/pdfium.patch?h=megasync"
        'appstream.patch')
sha256sums=('SKIP'
            'SKIP'
            'ccb6acc9eb7dc67f08d0044ece9d7c8ea4cc9ffa50a2bbb4d21e30d22575bb9d'
            '3a03d20165ebfb523644229bd650a2d0e51506c9555a1a9b28c61d07b8f28d7a'
            '0df0d8422924a551a14240bd62d53f27c2c3832bf433573b38d452a1cd44bca6')

prepare() {
    cd "MEGAsync"
    git submodule init
    git config submodule.src/MEGASync/mega.url "../meganz-sdk"
    git submodule update

    # Appstream
    patch -p1 -i "${srcdir}/appstream.patch"

    cd "src/MEGASync/mega"
    patch -Np1 -i "../../../../pdfium.patch"
}

build() {
    # build sdk
    cd "MEGAsync/src/MEGASync/mega"
    ./autogen.sh
    ./configure \
        --disable-shared \
        --enable-static \
        --disable-silent-rules \
        --disable-curl-checks \
        --disable-megaapi \
        --with-cryptopp \
        --with-sodium \
        --with-zlib \
        --with-sqlite \
        --with-cares \
        --with-curl \
        --without-freeimage \
        --with-libuv \
        --disable-posix-threads \
        --disable-examples \
        --with-libzen \
        --with-libmediainfo \
        --prefix="${srcdir}/MEGAsync/src/MEGASync/mega/bindings/qt/3rdparty"

    # build megasync
    cd "../.."
    qmake-qt5 "CONFIG += FULLREQUIREMENTS" MEGA.pro
    lrelease-qt5 MEGASync/MEGASync.pro
    make
}

package () {
    cd "MEGAsync"
    install -Dm 644 LICENCE.md "${pkgdir}/usr/share/licenses/megasync/LICENCE"
    install -Dm 644 installer/terms.txt "${pkgdir}/usr/share/licenses/megasync/terms.txt"
    install -Dm 644 src/MEGASync/mega/LICENSE "${pkgdir}/usr/share/licenses/megasync/SDK-LICENCE"
    
    cd "src"
    mkdir -pm 755 "${pkgdir}/usr/bin"
    make INSTALL_ROOT="${pkgdir}" TARGET="${pkgdir}/usr/bin/megasync" install

    install -Dm 755 "MEGASync/megasync" "${pkgdir}/usr/bin/megasync"

    
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
}
