# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: AwesomeHaircut <jesusbalbastro at gmail com>
# Contributor: Mateusz Gozdek <mgozdekof@gmail.com>
# Contributor: Rein Fernhout <public@reinfernhout.xyz>
# Past Contributor: James An <james@jamesan.ca>

pkgbase=droidcam
pkgname=${pkgbase}-dev
pkgver=1.7.2
pkgrel=1
pkgdesc='A tool for using your android device as a wireless/usb webcam'
arch=('x86_64')
url="https://github.com/aramg/${pkgbase}"
license=('GPL')
backup=("etc/modprobe.d/${pkgbase}.conf")
makedepends=('gtk3' 'libappindicator-gtk3' 'ffmpeg' 'libusbmuxd')
conflicts=("${pkgbase}" "${pkgbase}-dkms")
options=('!strip')
install="${pkgbase}.install"
source=("${pkgbase}-${pkgver}.zip::${url}/archive/v${pkgver}.zip"
        "${pkgbase}-modules.conf" "${pkgbase}-modprobe.conf"
        "https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgbase}/${pkgbase}"-{48,64,128}.png
        'appindicator.patch')

sha512sums=('cbb1ba79f450885c2d1598acd2d176ed1b86fe08db71efe9ed5050e90262659e6b7e55a8d0b7decc90b968fbfaaea835e3985f356700dd96b92839fa780f03ae'
            '715be186aff2eca7b383ca5bef00e34d08363fd688cea49080828e8b31db3b43d9605d4e2951ffdece9e80a94645e065189617f87337b0084946f20f61f76588'
            '25021201acb48aab6dc1d6cef7d117acd0b4e781d57ccebcfb9a1f574c2c56de470b318cc10df0ae156f1c1c7891282f0c618d87de2426b36301acdc775d0ece'
            '0548d8fd3ff57b24f371ab6dc61f0cf9a6450396cc7fcf08edd69360b8cd7d839286c22c9d1c094f57c310604f94c85982e34b451d4b0b752fcd93cff4919638'
            'a5f1fc7af095adcd715ad5b5bf109c6f2ccc03ba7897e4d520e55b1e7f8367967be87563f9c516ff0e1b0c76c79cfa07a964f61bc6f94ef38b62c6e788957fa5'
            '845054a565d5fd3921f7127e8d1948deaa028a63430c545647621cb5c5f7434add53cd93a89e2f926c081f905bffe8249cf14be5eedebb9126ac1c4fd37ceab2'
            '8974a76f9d25e114fea8c91e954be3364a1f1d392f034919fc3a63bec46811f8b2bba3fd1eb1ac0de51622c5435a398792e615db23c4c5d9d04fc936aeae8090'
            '3f8eb415ca381734c547b9d0d382d070c6eb2ac11547143a2dd10f8e30ea153037d94861987ee3284781426fad40b66f02a4ffe39c6129580a87cd457f9efcf1')

_droidcam_desktop="[Desktop Entry]
Encoding=UTF-8
Version=${pkgver}
Name=Droidcam
Type=Application
Exec=${pkgbase}
Icon=${pkgbase}
Categories=GNOME;AudioVideo;
Comment=A tool for using your Android phone as a wireless/usb webcam
Comment[pt_BR]=Uma ferramenta para usar seu telefone Android como uma webcam sem fio/usb"

prepare() {
    cd "${srcdir}/${pkgbase}-${pkgver}"
    patch -p1 -i ${srcdir}/appindicator.patch
}

build() {
    cd "${srcdir}"
    echo -e "$_droidcam_desktop" | tee com.${pkgbase}.desktop

    cd ${pkgbase}-${pkgver}

    # All JPEG* parameters are needed to use shared version of libturbojpeg instead of
    # static one.
    #
    # Also libusbmuxd requires an override while linking.
    make JPEG_DIR="" JPEG_INCLUDE="" JPEG_LIB="" JPEG=$(pkg-config --libs --cflags libturbojpeg) USBMUXD=-lusbmuxd-2.0
}

package() {
    depends=('alsa-lib' "v4l2loopback-dkms" 'ffmpeg' 'libjpeg-turbo' 'libusbmuxd' 'linux-headers' 'speex')
    optdepends=('gtk3: use GUI version in addition to CLI interface'
                'libappindicator-gtk3: use GUI version in addition to CLI interface')

    pushd ${pkgbase}-${pkgver}

    # Install droidcam program files
    mkdir -p ${pkgdir}/opt/${pkgbase}
    install -Dm755 ${pkgbase} "${pkgdir}/opt/${pkgbase}/${pkgbase}"
    install -Dm755 ${pkgbase}-cli "${pkgdir}/opt/${pkgbase}/${pkgbase}-cli"
    install -Dm644 "${srcdir}/${pkgbase}-modprobe.conf" "${pkgdir}/etc/modprobe.d/${pkgbase}.conf"
    install -Dm644 "${srcdir}/${pkgbase}-modules.conf" "${pkgdir}/etc/modules-load.d/${pkgbase}.conf"
    install -Dm644 icon2.png "${pkgdir}/opt/${pkgbase}-icon.png"

    # Symlink
    mkdir -p ${pkgdir}/usr/bin
    ln -s /opt/${pkgbase}/${pkgbase} "${pkgdir}/usr/bin/${pkgbase}"
    ln -s /opt/${pkgbase}/${pkgbase}-cli "${pkgdir}/usr/bin/${pkgbase}-cli"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgbase}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgbase}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgbase}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    sed -i "s/com.${pkgbase}.desktop/com.${pkgname/-/_}.desktop/" ${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgbase}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
    sed -i "/icon/ s/${pkgbase}/${pkgname}/" ${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml
}

