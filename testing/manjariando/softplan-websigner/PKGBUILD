# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=softplan-websigner
_pkgver=2.9.5
pkgver=${_pkgver}.1
pkgrel=2
pkgdesc="The Web Signer native application. An easy solution for using digital certificates in Web applications."
arch=('i686' 'x86_64')
url="https://websigner.softplan.com.br"
license=('custom')
groups=('')
depends=('desktop-file-utils' 'glib2' 'gtk3>=3.6' 'hicolor-icon-theme' 'xdg-utils')
#depends=('aarch64-linux-gnu-gcc' 'desktop-file-utils' 'glib2' 'gtk3>=3.6' 'hicolor-icon-theme' 'xdg-utils')
options=('!strip' '!emptydirs')
install=${pkgname}.install
source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png)
source_i686=("https://websigner.softplan.com.br/Downloads/${_pkgver}/webpki-chrome-32-deb")
source_x86_64=("https://websigner.softplan.com.br/Downloads/${_pkgver}/webpki-chrome-64-deb")
sha512sums=('10c10b445d0b1553903b265c836225d76bb87c2f8d8b42ad237966fb55d09a1115e03dba5c24d440d82b9c15e12a5a4d85367108fbdc8b639add577ca39c8512'
            '8e19072345336230de90dfb3c48fd473126b36a460384df14f1054a434f657b74ed5e058cb65af70f3271c7622b8131a7db184a9e17e0a8addbf48675999fd35'
            '89ba89b576fe32ebf3d4632d6f507df4705ab6411a054dd9f3e1c1c2ec7b04bc8e03571efa34e5dd094ff3ebbab392b46577e719576490e615565e78aeef65a0'
            '2cee991fbe4356be41e41ecdfdb3d66c9b5ad8d86fd58403666b294f72ba8ea502ee30763b7160fea0b40d0284328b763779ac06417bdde59c99f7a38619ee17')
sha512sums_i686=('c5d9a30f1a93fa1f75ef10e9632ed6d30b59a19fb1b50dbdd27f6effd6a841f1f903a790a21cdde4f7713dfc745961e0477091f7e321c73f508a5649658ee023')
sha512sums_x86_64=('a57b50c3f6f8f38282b4b1f9a39f7447c9fd87d4e1da549248e9657c201502cd9db035a3e59e7f3326e46a903ae7f6973e7e084b6fb2f38c3944079cdcd71372')

_softplan_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_softplan_desktop" | tee com.${pkgname/-/_}.desktop
}

package(){

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    if [[ ${CARCH} == x86_64 ]]; then
        rm -rf "${pkgdir}"/usr/lib/*; mv "${pkgdir}"/usr/lib64/* "${pkgdir}"/usr/lib; rm -rf "${pkgdir}"/usr/lib64
    fi

    if [[ ${CARCH} == i686 ]]; then
        rm -rf "${pkgdir}"/usr/lib64/*
    fi

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    # Archify folder permissions
    cd ${pkgdir}
    for d in $(find . -type d); do
        chmod 755 $d
    done
}
