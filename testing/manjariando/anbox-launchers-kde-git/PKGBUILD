# Maintainer: Ahmad Hasan Mubashshir <ahmubashshir@gmail.com>

_pkgname=anbox-launchers
pkgname=${_pkgname}-kde-git
pkgver=r14
pkgrel=1.1
pkgdesc="Add Anbox App Launchers to Anbox Category in Desktop Menu."
arch=('any')
url="https://github.com/ahmubashshir/anbox-launchers"
license=('GPL')
groups=()
makedepends=('coreutils' 'git')
install=${_pkgname}.install
source=("git+https://github.com/ahmubashshir/${_pkgname}.git"
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${_pkgname%%-launchers}/${_pkgname%%-launchers}"-{48,64,128}.png
        "anbox-android.menu")
validpgpkeys=('916961EE198832DD70B628B356DB0538F60D951C')
sha256sums=('SKIP'
            '5899510144070b608d3b5224bd0c4be12fafc82eda92229fe8d4df6299e1c8ca'
            '48b805dfd23bbae2aea6bc1cda569f3d2c2015d1b3b8ca95d502bf42e7839d4b'
            'a25deeaae2f7ab28f21ef16b716761ff5bdd5c77f3f8aa919c0c854315add01c'
            '20ebc3eda21aa2834b2978ce251b71c50a3d949417e4e91a0f0e792682da1dcf'
            '74f9508bbc14bf00d51bbba0250f8c65c56adf39caee26b67dbd2284df858185')

pkgver() {
    cd "${srcdir}/${_pkgname}"
    printf "r%s" "$(git rev-list --count HEAD)"
}

_anbox_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build () {
    cd "${srcdir}"
    echo -e "$_anbox_desktop" | tee com.${_pkgname}.desktop
}

package() {
    depends=('anbox' 'systemd' 'python-xdg')
    provides=('anbox-launchers')
    conflicts=('anbox-launchers')
    
    cd "${srcdir}/${_pkgname}"
    make DESTDIR="${pkgdir}" SYSCONFDIR=/etc install

    # Fix
    rm -rf "${pkgdir}/etc/xdg/menus/applications-merged/anbox-android.menu"
    install -Dm644 "${srcdir}/anbox-android.menu" "${pkgdir}/etc/xdg/menus/applications-merged/anbox-android.menu"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${_pkgname/-/_}_kde.desktop"
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "$pkgdir/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    sed -i "/name/ s/Anbox Launchers/Anbox Launchers KDE/" ${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml
    sed -i "/name/ s/Lançadores Anbox/Lançadores Anbox KDE/" ${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml
    sed -i "s/com.anbox_launchers.desktop/com.anbox_launchers_kde.desktop/" ${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${_pkgname%%-launchers}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done
}
