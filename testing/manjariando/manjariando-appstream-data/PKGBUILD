# Based on the file created for Arch Linux by:
# Antonio Rojas <arojas@archlinux.org>

# Maintainer: tioguda <guda.flavio@gmail.com>

_pkgname=manjariando
pkgname=${_pkgname}-appstream-data
pkgver=20210315.1128
pkgrel=1
pkgdesc="Application database made available by Manjariando blog to AppStream-based software centers."
arch=('x86_64')
url="https://appstream.manjariando.com.br"
license=(GPL-3.0)

_source_x86_64="https://appstream-data.manjariando.com.br/export/data/Manjaro"
source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
          "https://metainfo.manjariando.com.br/${pkgname}/${_pkgname}"-{48,64,128}.png)

for _repo in ${_pkgname} ${_pkgname}-deps ${_pkgname}-game ${_pkgname}-legacy ${_pkgname}-themes; do
 source_x86_64+=(${_repo}-x86_64.xml.gz::${_source_x86_64}/${_repo}/Components-x86_64.xml.gz
          ${_repo}-icons-48x48-x86_64.tar.gz::${_source_x86_64}/${_repo}/icons-48x48.tar.gz
          ${_repo}-icons-64x64-x86_64.tar.gz::${_source_x86_64}/${_repo}/icons-64x64.tar.gz
          ${_repo}-icons-128x128-x86_64.tar.gz::${_source_x86_64}/${_repo}/icons-128x128.tar.gz)
 noextract+=(${_repo}-${CARCH}.xml.gz ${_repo}-icons-{48x48,64x64,128x128}-${CARCH}.tar.gz)
done

sha256sums=('dbea6077fb92a1ddc54e40b730385cf7a5a321c27af762bfce99cab91980f8c5'
            '68e26df7903e91446d41198430e62cc41de4723d21f17ebe194d125e3be11ce6'
            '8730b97c3de2a51cf59e790f4c12846a024910ed23f2494061ca16108e597892'
            '1338a70176eefec2bbb7592a26f55008397506fed2bd9cebc3008d39a793506d')
sha256sums_x86_64=('d835522c7c4b0efefb5ab9bcd0355c079f429ce2de2420ed0cc1c6404bb1a794'
                   '622281c03ed70e0955c41a5f8451f15db8f3395ef4214c270cbeb7132a70ea69'
                   '22e28216758a994493ec062d8eea1cd3baabb4c8b634bc85b226cc59fd3728ce'
                   'c695f59cf87df90b2befc2715c3636ec61cbd343802d7d6e3364979f5a23579f'
                   'abf18a3db15bcd6febe43d534d73163bf47c81faf0f297e3966e19b18d783874'
                   '2f6138e1a755e74c21c945dfb42887d4fb581d4713bab90382f2d5e65e014ac7'
                   '835d9e09310d4e254064702b23be8956a51e82c6d5f3898696344785502b3c62'
                   'e0df897e4cd9c9119dc85b630705272c72a0f4b65dbade71f9a465dd13baea0c'
                   'ce0bb5369cc33f5ebb4226bead2f68e689f59a3840a7e5a55a5c272c17eb9f94'
                   '2452b18947825cd5345aa434c698d400d16b18e19357c65ecd6badca22f063b3'
                   '31bb8ebbd5af5daa02ba1462f941e5b8c0e53a0a90539cc6b18f124f5f463551'
                   '9df4564044203e05d73269760f4d9b2d1398e007eb57ae148a0c80e601e14e35'
                   '7667a7c5e8d623a40707e584db58dfea2de5c877fcff5a072607c7871e84b8f5'
                   '50c5778a9de9c6f353cb9afda6839b15c60269bd6583aa1503caa2ac26b26616'
                   '358a6e336e728bd57367a131c052c2cbfcfd471890c1d76cc1ee38485c868da7'
                   '569a50c7b46859c177619ffdfafac71ecf1624c4e890415c477739d79aa05560'
                   '773659d199e5737009b1e4ea3f7a6a1eed0d26c324985bf49e15bd35e092f3bc'
                   '4a963923c42aad522699606effcd54c28a4b06210e7cbc98dd62248a00d43398'
                   '6ba7290483074ed18b3f17f7be8b98da362e39ba34b70971d0990db5133f2bde'
                   '7749b3a618ebf3493c689fa176d64a64a8537955877695476d4539ef0584edb4')

pkgver() {
  _date=$(date +%Y%m%d.%H%M)
  echo ${_date}
}

_manjariando_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_manjariando_desktop" | tee com.${pkgname//-/_}.desktop
}

package() {
    depends=( 'manjariando-mirrors')

    mkdir -p "${pkgdir}"/usr/share/app-info/{icons/archlinux-arch-{${_pkgname},${_pkgname}-deps,${_pkgname}-game,${_pkgname}-legacy,${_pkgname}-themes}/{48x48,64x64,128x128},xmls}
    for _repo in ${_pkgname} ${_pkgname}-deps ${_pkgname}-game ${_pkgname}-legacy ${_pkgname}-themes; do
    tar -xzf ${_repo}-icons-48x48-${CARCH}.tar.gz -C "${pkgdir}"/usr/share/app-info/icons/archlinux-arch-${_repo}/48x48
    tar -xzf ${_repo}-icons-64x64-${CARCH}.tar.gz -C "${pkgdir}"/usr/share/app-info/icons/archlinux-arch-${_repo}/64x64
    tar -xzf ${_repo}-icons-128x128-${CARCH}.tar.gz -C "${pkgdir}"/usr/share/app-info/icons/archlinux-arch-${_repo}/128x128
    install -m644 ${_repo}-${CARCH}.xml.gz "${pkgdir}"/usr/share/app-info/xmls/${_repo}.xml.gz
    done
        
    # Appstream
    install -d ${pkgdir}/usr/share/licenses/${pkgname}
    ln -s /usr/share/licenses/repo-manjariando/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${_pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
