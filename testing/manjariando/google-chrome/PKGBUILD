# Maintainer: Knut Ahlers <knut at ahlers dot me>
# Contributor: Det <nimetonmaili g-mail> 
# Contributors: t3ddy, Lex Rivera aka x-demon, ruario

# Check for new Linux releases in: http://googlechromereleases.blogspot.com/search/label/Stable%20updates
# or use: $ curl -s https://dl.google.com/linux/chrome/rpm/stable/x86_64/repodata/other.xml.gz | gzip -df | awk -F\" '/pkgid/{ sub(".*-","",$4); print $4": "$10 }'

pkgname=google-chrome
pkgver=89.0.4389.114
pkgrel=1
pkgdesc="The popular and trusted web browser by Google (Stable Channel)"
arch=('x86_64')
url="https://www.google.com/chrome"
license=('custom:chrome')
options=('!emptydirs' '!strip')
install=${pkgname}.install
_channel=stable
source=("http://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-${_channel}/google-chrome-${_channel}_${pkgver}-1_amd64.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/google-chrome.appdata.xml"
        "eula_text.html::https://aur.archlinux.org/cgit/aur.git/plain/eula_text.html?h=google-chrome"
        "google-chrome-${_channel}.sh")
sha512sums=('33813521e2ad77f261d91ce3d13480c76c7d4d4e58b2d3f1dc2201c9aa0caed51ea4e4470a539b39d6f4ce38d11c318243e02642d44979c636a8e83d12f79a10'
            'a05d6f9e67501a6b132805cd64114cdf817358657830f01d93f428cfe25799b7470fcfb607791e866ee7d3372241f37d60e4e7dfcf4504be2cca82a654c6e213'
            'a225555c06b7c32f9f2657004558e3f996c981481dbb0d3cd79b1d59fa3f05d591af88399422d3ab29d9446c103e98d567aeafe061d9550817ab6e7eb0498396'
            '956642602416b97fba999da29d64e2590eb21440b2b371913d3086676fba7235c7189c8d5994c3d2b80b1bf408feb75a72c69ff5aa35a08870bfc71c58886ecc')

package() {
    depends=('alsa-lib' 'gtk3' 'libcups' 'libxss' 'libxtst' 'nss')
    optdepends=('kdialog: for file dialogs in KDE'
                'gtk3-print-backends: for printing'
                'kwallet: for storing passwords in KWallet'
                'libpipewire02: WebRTC desktop sharing under Wayland'
                'gnome-keyring: for storing passwords in GNOME keyring'
                'ttf-liberation: fix fonts for some PDFs (CRBug #369991)'
                'plasma-browser-integration: for integrate browsers into the Plasma Desktop'
                'xdg-utils')

    msg2 "Extracting the data.tar.xz..."
    bsdtar -xf data.tar.xz -C "${pkgdir}/"

    msg2 "Moving stuff in place..."
    # Launcher
    install -Dm755 google-chrome-${_channel}.sh "${pkgdir}"/usr/bin/google-chrome-${_channel}

    # Icons
    for i in 16x16 24x24 32x32 48x48 64x64 128x128 256x256; do
        install -Dm644 "${pkgdir}"/opt/google/chrome/product_logo_${i/x*}.png \
                    "${pkgdir}"/usr/share/icons/hicolor/$i/apps/google-chrome.png
    done

    # License
    install -Dm644 eula_text.html "${pkgdir}"/usr/share/licenses/google-chrome/eula_text.html

    msg2 "Fixing Chrome icon resolution..."
    sed -i \
        -e "/Exec=/i\StartupWMClass=Google-chrome" \
        -e "s/x-scheme-handler\/ftp;\?//g" \
            "${pkgdir}"/usr/share/applications/google-chrome.desktop

    msg2 "Removing Debian Cron job and duplicate product logos..."
    rm -r "${pkgdir}"/etc/cron.daily/ "${pkgdir}"/opt/google/chrome/cron/
    rm "${pkgdir}"/opt/google/chrome/product_logo_*.png

    # Appstream
    rm -rf "${pkgdir}/usr/share/appdata"
    install -Dm644 "${srcdir}/google-chrome.appdata.xml" "${pkgdir}/usr/share/metainfo/google-chrome.appdata.xml"
}
