# Maintainer: Muflone http://www.muflone.com/contacts/english/

pkgname=4kyoutubetomp3
pkgver=3.15.0.4160
pkgrel=1
pkgdesc="Extract audio from YouTube, Vimeo, Facebook and other online video hosting services."
arch=('x86_64')
url="http://www.4kdownload.com/products/product-youtubetomp3"
license=('custom:eula')
makedepends=('chrpath')
#install=${pkgname}.install
source=("${pkgname}_${pkgver}_amd64.tar.bz2"::"https://dl.4kdownload.com/app/${pkgname}_${pkgver%.*}_amd64.tar.bz2"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png
        "fix_symlink_path.patch")
sha256sums=('13da9bc32180ffd00f6494899cbd780cf7176848f53a71f8c871ae261bacb649'
            '03cc1d130b06c7953ee3d3c9565d22a8cf620261c5946d2b2bedf9607c07baa1'
            'b25f830bb1fe559ea9f0b35cc9eb8ab75e2e40d09b8755f937451f5ddeeec2fd'
            '295863b015c08da994e304323d60b33d0cba9521ebb84f15de4f1ecbe605723f'
            '1614179ef021f9577124d09183c5ab4ec24cd8b55074ca91e9d921d58d62afe4'
            '1bc2c992e21bae6c51f3176f4c3e04577b3297ea98ffc45fb56ce802423cf6cb')

prepare() {
    cd "${pkgname}"
    # Remove insecure RPATH
    chrpath --delete "${pkgname}-bin"
    # Fix symlink path
    patch -p1 -i "${srcdir}/fix_symlink_path.patch"
}

_4kyoutubetomp3_desktop="[Desktop Entry]
Name=4K YouTube to MP3
Name[pt_BR]=4K YouTube para MP3
GenericName=4K YouTube to MP3
GenericName[pt_BR]=4K YouTube para MP3
Comment=Download online audio
Comment[pt_BR]=Download de áudio online
Exec=4kyoutubetomp3
Terminal=false
Type=Application
Icon=4kyoutubetomp3
Categories=AudioVideo;Network;Qt;"

build() {
    cd "${srcdir}"
    echo -e "$_4kyoutubetomp3_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=('qt5-script' 'qt5-declarative' 'portaudio' 'ffmpeg' 'openssl-1.0')

    # Install files
    cd "${pkgname}"
    install -m 755 -d "${pkgdir}/usr/lib/${pkgname}"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "${pkgname}-bin"
    install -m 755 -d "${pkgdir}/usr/lib/${pkgname}/audio"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}/audio" audio/*
    install -m 755 -d "${pkgdir}/usr/lib/${pkgname}/translation"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}/translation" translation/*
    # Temporarily ship bundled QT5 libraries as system libraries are unsupported
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "${pkgname}.sh"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Concurrent.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Core.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5DBus.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Gui.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Network.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Positioning.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5PrintSupport.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Qml.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5QmlModels.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Quick.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5QuickWidgets.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Widgets.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5WebChannel.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5WebEngineCore.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5WebEngineWidgets.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5XcbQpa.so.5"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}" "libQt5Xml.so.5"
    install -m 755 -d "${pkgdir}/usr/lib/${pkgname}/platforms"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}/platforms" platforms/*
    install -m 755 -d "${pkgdir}/usr/lib/${pkgname}/xcbglintegrations"
    install -m 755 -t "${pkgdir}/usr/lib/${pkgname}/xcbglintegrations" xcbglintegrations/*

    # Install launcher file
    install -m 755 -d "${pkgdir}/usr/bin"
    ln -s "/usr/lib/${pkgname}/${pkgname}-bin" "${pkgdir}/usr/bin/${pkgname}"

    # Install license file
    install -m 755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
    install -m 644 -t "${pkgdir}/usr/share/licenses/${pkgname}" "doc/eula"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
