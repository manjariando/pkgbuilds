# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Arthur Heymans <arthur@aheymans.xyz>

_pkgname=intel-ucode-platomav
pkgname=${_pkgname}-git
pkgver=r130
pkgrel=1
pkgdesc='Microcode update files for Intel CPUs'
arch=('any')
url='https://github.com/platomav/CPUMicrocodes'
license=('custom')
replaces=('microcode_ctl')
conflicts=('intel-ucode')
makedepends=('iucode-tool' 'git')
install=${_pkgname}.install

source=("git+https://github.com/platomav/CPUMicrocodes.git"
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${_pkgname}/cpu-intel"-{48,64,128}.png
        "LICENSE")
sha256sums=('SKIP'
            '744e7e4c7b02bab8e79538023432f65b73dfdaedb2b77f6b5f8d37b31e590adc'
            'bc576b2f19d60d25ab99da565b29a1770bb8d4a788505a1bac2fdc3098f145aa'
            '6006ad330b77e6d81088c4db00acd483e4ad98711addd7fc19f7571738dd42f9'
            '243b6572187d6215a9e46b895c7e8f8195a1e47906b24b0edfda8d32cdae0b7d'
            '6983e83ec10c6467fb9101ea496e0443f0574c805907155118e2c9f0bbea97b6')

invalid=('cpu106C0_plat01_ver00000007_2007-08-24_PRD_923CDFA3.bin')

pkgver() {
    cd "$srcdir"/CPUMicrocodes
    printf "r%s" "$(git rev-list --count HEAD)"
}

prepare() {
    cd "${srcdir}"
    rm -rf kernel/x86/microcode/GenuineIntel.bin
    rm -rf CPUMicrocodes/Intel/LICENSE

    for i in $invalid
    do
        msg2 'Removing invalid microcode %s' $i
        rm CPUMicrocodes/Intel/$i
    done

    mkdir -p kernel/x86/microcode
    iucode_tool -w kernel/x86/microcode/GenuineIntel.bin CPUMicrocodes/Intel
    echo kernel/x86/microcode/GenuineIntel.bin | bsdcpio -o -H newc -R 0:0 > intel-ucode.img
}

_ucode_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_ucode_desktop" | tee com.${_pkgname//-/_}.desktop
}

package() {
    cd "${srcdir}"

    install -D -m0644 intel-ucode.img ${pkgdir}/boot/intel-ucode.img
        
    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname//-/_}.desktop" "${pkgdir}/usr/share/applications/com.${_pkgname//-/_}.desktop"
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/${_pkgname}/LICENSE"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/cpu-intel-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done
}
