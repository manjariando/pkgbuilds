# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Alex Taber <aft dot pokemon at gmail dot com>

pkgname=teamviewer
pkgver=15.16.8
pkgrel=1
pkgdesc='All-In-One Software for Remote Support and Online Meetings'
arch=('i686' 'x86_64') # 'armv7h')
url='https://www.teamviewer.com'
license=('custom')
makedepends=('imagemagick')
options=('!strip')
provides=("${pkgname}")
conflicts=("${pkgname}-beta")
install=${pkgname}.install
source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.TeamViewer.metainfo.xml")
source_x86_64=("https://dl.teamviewer.com/download/linux/version_${pkgver%%.*}x/${pkgname}_${pkgver}_amd64.deb")
source_i686=("https://dl.teamviewer.com/download/linux/version_${pkgver%%.*}x/${pkgname}_${pkgver}_i386.deb")
#source_armv7h=("https://dl.tvcdn.de/download/linux/version_${pkgver%%.*}x/teamviewer-host_${pkgver}_armhf.deb")
#source_armv7h=("https://dl.tvcdn.de/download/linux/version_${pkgver%%.*}x/teamviewer-host_13.2.13582_armhf.deb")
sha256sums=('bfa99b5c2ee963cedc9633e324becc470e63d207fb961ac4373e4a71f2ae7c0f')
sha256sums_i686=('b2b55345ae9dbe7c9ef085b2eeb97506589e13bbbf01fc4d2f76e1ed5857aabc')
sha256sums_x86_64=('8f53adc74baa433565a64e3cf9471660d0ee7af881cdbb92ed88918accf49827')
#sha256sums_armv7h=('b78e7b9f6ec6e510fba503ff62ef10f09c1b8deee657fd81f11334e6616ba43a')

prepare() {
    warning "If the install fails, you need to uninstall previous major version of Teamviewer"
    [ -d data ] && rm -rf data
    mkdir data
    cd data
    for datatar in ../data.tar.*; do
        msg2 "Unpacking ${datatar}"
        tar -xf ${datatar}
    done
    sed -i '/function CheckQtQuickControls()/{N;a ls /usr/lib/qt/qml/QtQuick/Controls/qmldir &>/dev/null && return # ManjaroLinux
}' ./opt/teamviewer/tv_bin/script/teamviewer_setup || msg2 "Patching CheckQtQuickControls failed! Contact maintainer"
}

package() {
    depends=('hicolor-icon-theme'
            'qt5-webkit'
            'qt5-x11extras'
            'qt5-quickcontrols')

    # Install
    warning "If the install fails, you need to uninstall previous major version of Teamviewer"
    cp -dr --no-preserve=ownership ./data/{etc,opt,usr,var} "${pkgdir}"/

    # Additional files
    rm "${pkgdir}"/opt/teamviewer/tv_bin/xdg-utils/xdg-email
    rm -rf "${pkgdir}"/etc/apt
    install -D -m0644 "${pkgdir}"/opt/teamviewer/tv_bin/script/teamviewerd.service \
        "${pkgdir}"/usr/lib/systemd/system/teamviewerd.service
    install -d -m0755 "${pkgdir}"/usr/{share/applications,share/licenses/teamviewer}
    ln -s /opt/teamviewer/License.txt \
        "${pkgdir}"/usr/share/licenses/teamviewer/LICENSE
    if [ "$CARCH" = "x86_64" ] && [ -f "${pkgdir}/opt/${pkgname}/tv_bin/script/libdepend" ]; then
        msg2 "Removing libdepend to ditch lib32 dependencies"
        rm "${pkgdir}/opt/${pkgname}/tv_bin/script/libdepend"
    fi

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.TeamViewer.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.TeamViewer.metainfo.xml"

    # Fix and install desktop icons
    for size in 22 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/opt/teamviewer/tv_bin/desktop/teamviewer_256.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/TeamViewer.png"
    done
}
