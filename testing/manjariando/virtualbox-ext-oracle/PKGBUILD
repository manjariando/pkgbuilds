# Maintainer: Sébastien Luttringer

pkgname=virtualbox-ext-oracle
_version=6.1.18
pkgver=${_version/-/.}
pkgrel=1
pkgdesc='Oracle VM VirtualBox Extension Pack'
arch=('any')
url='https://www.virtualbox.org'
license=('custom:PUEL')
options=('!strip')
install=virtualbox-ext-oracle.install
source=("https://download.virtualbox.org/virtualbox/${pkgver}/Oracle_VM_VirtualBox_Extension_Pack-${pkgver}.vbox-extpack"
        "https://metainfo.manjariando.com.br/${pkgname}/org.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/virtualbox"-{48,64,128}.png)
noextract=("Oracle_VM_VirtualBox_Extension_Pack-${pkgver}.vbox-extpack")
sha256sums=('d609e35accff5c0819ca9be47de302abf094dc1b6d4c54da8fdda639671f267e'
            'eba4d6888b35fe2dc391176e299191517d60af9644ef15b07b898bade8130fad'
            'f13bc62e308e37a0485fd11dd3d3520e9265d1d51dc94240b3f959d1ca165f6a'
            'bf2d8a77421e67a5ea852870be6d4bb7f67db95fba59d3b8370b95243c25c9d3'
            'ab92676d516d1b3b45f9186d2d58d4c8dc32be9f3a416df5b79cad8e69a15b07')

prepare() {
  # shrink uneeded cpuarch
  [[ -d shrunk ]] || mkdir shrunk
  tar xfC "Oracle_VM_VirtualBox_Extension_Pack-${pkgver}.vbox-extpack" shrunk
  rm -r shrunk/{darwin*,solaris*,win*}
  tar -c --gzip --file shrunk.vbox-extpack -C shrunk .
}

_virtualbox_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_virtualbox_desktop" | tee org.${pkgname//-/_}.desktop
}

package() {
    depends=('virtualbox')
    optdepends=('rdesktop: client to connect vm via RDP')

    install -Dm 644 shrunk.vbox-extpack \
        "${pkgdir}/usr/share/virtualbox/extensions/Oracle_VM_VirtualBox_Extension_Pack-${pkgver}.vbox-extpack"
    install -Dm 644 shrunk/ExtPack-license.txt \
        "${pkgdir}/usr/share/licenses/$pkgname/PUEL"

    # Appstream
    install -Dm644 "${srcdir}/org.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/org.${pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/org.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/virtualbox-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
