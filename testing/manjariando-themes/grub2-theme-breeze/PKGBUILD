# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=grub2-theme-breeze
pkgver=5.20.3
pkgrel=1.1
pkgdesc="A minimalistic GRUB theme inspired by Breeze."
arch=("any")
url="https://www.gnome-look.org/p/1000140"
license=('CC BY-SA')
install=grub-theme.install
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/gustawho/${pkgname}/archive/${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/grub"-{48,64,128}.png)
sha256sums=('0431817cc653ff112b4b7b9dbafe3267e9c1e12e44a8bbd48ca2d5019301cd2e'
            '531faf32f16975b9cdcad3ec9ee2a7896f83143a4155b3baca740d7632ef07c6'
            'ec022f3d4c8ff9c6942773c199afcd8e487769e95f887da58649a097acf94858'
            'b013684ded755a4ad7029f70f51d8acafe494d7b9f8772674f6e7510791d043a'
            'b013684ded755a4ad7029f70f51d8acafe494d7b9f8772674f6e7510791d043a')

prepare() {
    cd ${srcdir}/${pkgname}-${pkgver}
}

_breeze_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_breeze_desktop" | tee com.${pkgname//-/_}.desktop
}

package() {
    depends=('grub')

    cd ${srcdir}/${pkgname}-${pkgver}
    install -dm755 ${pkgdir}/boot/grub/themes/breeze
    cp -r breeze/* ${pkgdir}/boot/grub/themes/breeze
    
    # Fix icon to dual boot with two versions of Manjaro
    cp -r ${pkgdir}/boot/grub/themes/breeze/icons/manjaro.png ${pkgdir}/boot/grub/themes/breeze/icons/manjarolinux.png

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/${pkgname}-${pkgver}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/grub-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:PACKAGE=.*:PACKAGE=breeze:" "${startdir}/grub-theme.install"
}
