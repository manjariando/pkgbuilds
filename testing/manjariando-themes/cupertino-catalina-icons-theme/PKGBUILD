# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Chanathip Srithanrat <axesd9@gmail.com>

_pkgname=Cupertino-Catalina
pkgname=cupertino-catalina-icons-theme
pkgver=6.4
pkgrel=1
pkgdesc='Cupertino Catalina iCons Collection'
arch=('any')
url='https://store.kde.org/p/1102582'
license=('LGPLv3')
makedepends=('git')
conflicts=('cupertino-icons-theme-dev')
_commit=7c745cc98a2c52d1aefde1b123054d50ec851be0
source=("git+https://github.com/USBA/${_pkgname}-iCons.git#commit=$_commit"
        "https://www.gnu.org/licenses/lgpl-3.0.txt"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png)
sha256sums=('SKIP'
            'e3a994d82e644b03a792a930f574002658412f62407f5fee083f2555c5f23118'
            '0574ca6cda732d6fc6d28ef8f34fa34bc2f8548e09bc4dc5f5de490a00002a18'
            'c76bfd7d6255e177567049e5cfe87d0648e0c3660e89350ef5b6a26ec6917827'
            'f117997c9587d4e09b0b566f21ef8dffa31878bb563ac2d34830bf075551093b'
            '8964bec97145c10c8891c60a90c86fd937bf76543cd22fb2fdd4b43b97b63633')

prepare() {
    find -name '* *' -delete
    mv ${_pkgname}-iCons ${_pkgname}
    cp ${_pkgname}/status/symbolic/*.svg ${_pkgname}/devices/symbolic
    cp ${_pkgname}/apps/128/accessories_calculator.png ${_pkgname}/apps/128/gnome-calculator.png
}

_cupertino_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_cupertino_desktop" | tee com.cupertino_catalina_icons_theme.desktop
}

package() {
    depends=('gtk-update-icon-cache')

    find */ -type f -exec install -Dm644 '{}' ${pkgdir}/usr/share/icons/'{}' \;

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/lgpl-3.0.txt" "${pkgdir}/usr/share/licenses/${_pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.cupertino_catalina_icons_theme.desktop" \
        "${pkgdir}/usr/share/applications/com.cupertino_catalina_icons_theme.desktop"
    rm -rf ${pkgdir}/usr/share/icons/${_pkgname}/.{git,DS_Store,gitattributes}

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
