# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: holoitsme <bisio.97@gmail.com> #https://github.com/holoitsme/manjaro-bootsplash-maker/commit/056460ed66302aa470b72947f036724bb7d196d1

pkgbase=mac
pkgname=bootsplash-theme-${pkgbase}
pkgver=20210222
pkgrel=1
url="https://manjariando.com.br/2020/11/13/bootsplash-mac-os"
arch=('any')
license=('GPL')
makedepends=('imagemagick')
install=bootsplash-theme.install
options=('!libtool' '!emptydirs')

source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/bootsplash-theme-manjariando/boot"-{48,64,128}.png
        'bootsplash-packer'
        'bootsplash-mac.sh'
        'bootsplash-mac.initcpio_install'
        'spinner.gif'
        'logo.png')

sha256sums=('18a603b88235bf3115f460425a3c6bd11fe9493c38271cbf0fc79d28d6d9c04e'
            'db12910980a9669036b4bfde58638b5349f011a5dbf808be1d617a5cff13d058'
            'df4c3d60e860f3fb129cb372380864060cd197d49bfe05f9cf8871001fbf36f2'
            '1857532b9c74ef8e24ceb79050bea2954bc377a632eaabb83c83c375df616a29'
            '51559d3ccfb448b03fa6439faf5869dbd0c2fbda1b5d5bf5d4ba70e60937472a'
            'd003fc5a64dedfae21340f6af1cc28cfbe90dbb3f8a201f58f2013118d8e8df2'
            '5ff221e11acdea45fff4d7b56170fef950e0df034d94b9405894280ccddd7adf'
            '360fa527f2ffb40933848e7aee43004921dac3c0796a96c7c2aa585df6f10365'
            'e82261b5c3c5d780ea48b45898ab26acb8cb1ba136871e052f83436fd55f5453')

pkgver() {
    _date=$(date +%F | sed s@-@@g)
    echo ${_date}
}

_bootsplash_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_bootsplash_desktop" | tee com.${pkgname//-/_}.desktop
    sh ./bootsplash-mac.sh
}

package() {
    pkgdesc="Bootsplash Theme Mac OS logo"
    depends=('bootsplash-manager')

    cd "$srcdir"

    install -Dm644 "$srcdir/bootsplash-mac" "$pkgdir/usr/lib/firmware/bootsplash-themes/mac/bootsplash"
    install -Dm644 "$srcdir/bootsplash-mac.initcpio_install" "$pkgdir/usr/lib/initcpio/install/bootsplash-mac"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -d ${pkgdir}/usr/share/licenses/${pkgname}
    ln -s /usr/share/licenses/repo-manjariando/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/boot-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:THEME=.*:THEME=${pkgbase}:" "${startdir}/bootsplash-theme.install"
} 
