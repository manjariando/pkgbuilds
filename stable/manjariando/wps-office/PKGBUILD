# Maintainer: Ouyang Jun <ouyangjun1999@gmail.com>
# Contributor: Astro Benzene <universebenzene at sina dot com>
# Contributor: Felix Yan <felixonmars@archlinux.org>
# Contributor: Jove Yu <yushijun110 [at] gmail.com>
# Contributor: Ariel AxionL <axionl at aosc dot io>

pkgbase=wps-office
pkgname=("${pkgbase}" "${pkgbase}-mime")
pkgver=11.1.0.10161
pkgrel=2
_pkgrel=1
pkgdesc="Kingsoft Office (WPS Office) is an office productivity suite"
arch=('x86_64')
license=("custom")
url="https://www.wps.com"
options=('!strip' '!emptydirs')
source=("https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}"-{et,pdf,wpp,wps,mime,prometheus}.metainfo.xml)
source_x86_64=("http://wdl1.pcfg.cache.wpscdn.com/wpsdl/wpsoffice/download/linux/${pkgver##*.}/wps-office-${pkgver}.XA-${_pkgrel}.x86_64.rpm")
sha256sums=('7ad5fdaac76dbb53ebe4929183881381c5f5bff00418e55f49fe6c5d01699a57'
            '11ab1a885ffa984dac94eb5fc874246bdffea1066938e73b19c330fdf86f7844'
            '5c41647a3006f6095d8280c752317976d050c5f7993f46fb8af64a4e2877ad7a'
            'aafc5313f1106ae480e519e078ce5f9b784bb9ceb0ad3335db6a140ce05ef7b0'
            'd986b0fa0fd53a8a7dc5503654862ee1824feb35754ff088cebb3120f74bf547'
            'a3f5fce061986c0dfa386b5585c0d9c78a032e41433d2d6fbd383e468407164a')
sha256sums_x86_64=('d8d16c3eea4bb902c12a79866072754ee460adba16fcc79a1f52eae011cf462d')

prepare() {
    #bsdtar -xpf data.tar.xz

    cd "${srcdir}/usr/bin"
    sed -i 's|/opt/kingsoft/wps-office|/usr/lib|' *

    cd "${srcdir}/usr/share/icons/hicolor"

    for _file in ./*; do
        if [ -e ${_file}/mimetypes/wps-office2019-etmain.png ]; then
            mkdir -p ${_file}/apps
            cp -p ${_file}/mimetypes/wps-office2019* ${_file}/apps
        fi
    done
}

_wps_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_wps_desktop" | tee com.${pkgbase/-/_}_mime.desktop
}

package_wps-office() {
    depends=('fontconfig' 'xorg-mkfontdir' 'libxrender' 'desktop-file-utils' 'shared-mime-info' 'xdg-utils' 'glu' 'openssl-1.0' 'sdl2' 'libpulse' 'hicolor-icon-theme' 'gtk2' 'libxss' 'sqlite' 'ttf-wps-fonts' 'libtool')
    optdepends=('cups: for printing support'
                'libjpeg-turbo: JPEG image codec support'
                'pango: for complex (right-to-left) text support'
                'curl: An URL retrieval utility and library'
                'wps-office-fonts: FZ TTF fonts provided by wps community'
                'wps-office-mime: Use mime files provided by Kingsoft')
    conflicts=('kingsoft-office')
    install=${pkgname}.install
    cd "${srcdir}/opt/kingsoft/wps-office/"

    install -d "${pkgdir}/usr/lib"
    cp -r office6 "${pkgdir}/usr/lib"
    install -Dm644 -t "${pkgdir}/usr/share/licenses/${pkgname}" office6/mui/default/*.html

    install -d "${pkgdir}/usr/bin"
    cd "${srcdir}/usr/bin"
    install -m755 * "${pkgdir}/usr/bin"

    cd "${srcdir}/usr/share"

    install -d "${pkgdir}/usr/share/desktop-directories"
    cp -r desktop-directories/* "${pkgdir}/usr/share/desktop-directories"

    install -d "${pkgdir}/usr/share/icons"
    cp -r icons/* "${pkgdir}/usr/share/icons"
    
    install -Dm644 -t "${pkgdir}/usr/share/fonts/wps-office" fonts/wps-office/*

    install -Dm644 -t "${pkgdir}/etc/xdg/menus/applications-merged" "${srcdir}/etc/xdg/menus/applications-merged/wps-office.menu"

    # Appstream
    install -Dm644 "${srcdir}"/com.${pkgname}-wps.metainfo.xml "${pkgdir}/usr/share/metainfo/com.${pkgname}-prometheus.metainfo.xml"
    install -Dm644 "${srcdir}"/com.${pkgname}-wps.metainfo.xml "${pkgdir}/usr/share/metainfo/com.${pkgname}-wps.metainfo.xml"
    install -Dm644 "${srcdir}"/com.${pkgname}-pdf.metainfo.xml "${pkgdir}/usr/share/metainfo/com.${pkgname}-pdf.metainfo.xml"
    install -Dm644 "${srcdir}"/com.${pkgname}-wpp.metainfo.xml "${pkgdir}/usr/share/metainfo/com.${pkgname}-wpp.metainfo.xml"
    install -Dm644 "${srcdir}"/com.${pkgname}-et.metainfo.xml "${pkgdir}/usr/share/metainfo/com.${pkgname}-et.metainfo.xml"

    install -d "${pkgdir}/usr/share/applications"
    cp -r applications/wps-office-prometheus.desktop "${pkgdir}/usr/share/applications/com.wps_office_prometheus.desktop"
    cp -r applications/wps-office-wps.desktop "${pkgdir}/usr/share/applications/com.wps_office_wps.desktop"
    cp -r applications/wps-office-pdf.desktop "${pkgdir}/usr/share/applications/com.wps_office_pdf.desktop"
    cp -r applications/wps-office-wpp.desktop "${pkgdir}/usr/share/applications/com.wps_office_wpp.desktop"
    cp -r applications/wps-office-et.desktop "${pkgdir}/usr/share/applications/com.wps_office_et.desktop"

    sed -i '/et.desktop/ s/wps-office-et.desktop/com.wps_office_et.desktop/' "${pkgdir}/etc/xdg/menus/applications-merged/wps-office.menu"
    sed -i '/wps.desktop/ s/wps-office-wps.desktop/com.wps_office_wps.desktop/' "${pkgdir}/etc/xdg/menus/applications-merged/wps-office.menu"
    sed -i '/wpp.desktop/ s/wps-office-wpp.desktop/com.wps_office_wpp.desktop/' "${pkgdir}/etc/xdg/menus/applications-merged/wps-office.menu"
    sed -i '/pdf.desktop/ s/wps-office-pdf.desktop/com.wps_office_pdf.desktop/' "${pkgdir}/etc/xdg/menus/applications-merged/wps-office.menu"
    sed -i '/prometheus.desktop/ s/wps-office-prometheus.desktop/com.wps_office_prometheus.desktop/' "${pkgdir}/etc/xdg/menus/applications-merged/wps-office.menu"
}

package_wps-office-mime() {
    pkgdesc="Mime files provided by Kingsoft Office (WPS Office)"
    depends=('shared-mime-info' "${pkgbase}=${pkgver}")
    cd "${srcdir}/usr/share"

    install -d "${pkgdir}/usr/share/mime"
    cp -r mime/* "${pkgdir}/usr/share/mime"

    cd "${srcdir}/opt/kingsoft/wps-office/"
    install -Dm644 -t "${pkgdir}/usr/share/licenses/${pkgname}" office6/mui/default/*.html

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgbase/-/_}_mime.desktop" "${pkgdir}/usr/share/applications/com.${pkgbase/-/_}_mime.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/usr/share/icons/hicolor/${i}x${i}/mimetypes/wps-office-wps.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
