# Like `backup` in `PKGBUILD`, but supports folders – no leading and trailing slash here!
_BACKUP=('opt/lampp/etc' 'opt/lampp/htdocs' 'opt/lampp/var')

post_install() {
    if [ -e "/usr/share/hunspell/pt_BR.dic" ]
    then
    echo ""
    echo -e "\e[34;1m==>\e[39;1m O XAMPP agora está instalado no diretório /opt/lampp \e[0m"
    echo ""
    echo -e "\e[34;1m==>\e[39;1m Para iniciar, parar ou reiniciar o XAMPP, basta chamar o comando:\e[0m"
    echo -e "\e[34;1m==>\e[39;1m sudo xampp {start, stop, restart}\e[0m"
    echo ""
    echo -e "\e[34;1m==>\e[39;1m Então você pode verificar se tudo realmente funciona. \e[0m"
    echo -e "\e[34;1m==>\e[39;1m Basta digitar o seguinte URL no seu navegador: \e[0m"
    echo -e "\e[34;1m==>\e[39;1m http://localhost \e[0m"
    echo ""
    else
    echo ""
    echo -e "\e[34;1m==>\e[39;1m XAMPP is now installed below the /opt/lampp directory\e[0m"
    echo ""
    echo -e "\e[34;1m==>\e[39;1m To start, stop or restart XAMPP simply call the command:\e[0m"
    echo -e "\e[34;1m==>\e[39;1m sudo xampp {start, stop, restart}\e[0m"
    echo ""
    echo -e "\e[34;1m==>\e[39;1m Then you can check that everything really works. \e[0m"
    echo -e "\e[34;1m==>\e[39;1m Just enter the following URL at your web browser: \e[0m"
    echo -e "\e[34;1m==>\e[39;1m http://localhost \e[0m"
    echo ""
    fi
}

pre_upgrade() {
    local _STDERR
    local _COIN
    
    echo -n 'Stopping lampp services...'
    _STDERR="$(/opt/lampp/lampp stop 2>&1 >/dev/null)" && echo ' OK' || {
        echo ' FAILED'
        test "x${_STDERR}" != 'x' && echo "${_STDERR}" 1>&2
    }

    for _COIN in "${_BACKUP[@]}"; do
        ! test -d "/${_COIN}" || (rm -rf "/${_COIN}.keep" && mv "/${_COIN}" "/${_COIN}.keep")
    done
}

post_upgrade() {
    local _COIN
    local _HAVE_PACNEW=no

    for _COIN in "${_BACKUP[@]}"; do
        if test -d "/${_COIN}.keep"; then
            _HAVE_PACNEW=yes
            rm -rf "/${_COIN}.pacnew"
            mv "/${_COIN}" "/${_COIN}.pacnew"
            mv "/${_COIN}.keep" "/${_COIN}"
            echo "/${_COIN} has been installed as /${_COIN}.pacnew"
        fi
    done

    if test "x${_HAVE_PACNEW}" = 'xyes'; then
        echo
        echo -e "\e[34;1m==>\e[39;1m One or more files have been saved as .pacnew files to preserve a current \e[0m"
        echo -e "\e[34;1m==>\e[39;1m version. To discard these files launch: \e[0m"
        echo
        echo -e "\e[34;1m==>\e[39;1m sudo rm -r `sudo find -L '\''/opt/lampp/'\'' -name '\''*.pacnew'\''` \e[0m"
        echo
    fi
}


pre_remove() {
    local _STDERR
    local _COIN
    local _DATESTAMP="$(date '+%Y-%m-%d-%H-%M-%S')"

    if [ -e "/usr/share/hunspell/pt_BR.dic" ]
    then
    echo ""
    echo -e "\e[34;1m==>\e[39;1m Parando serviços de lampp...\e[0m"
    else
    echo ""
    echo -e "\e[34;1m==>\e[39;1m Stopping lampp services...\e[0m"
    fi

    _STDERR="$(/opt/lampp/lampp stop 2>&1 >/dev/null)" && echo ' OK' || {
        echo ' FAILED'
        test "x${_STDERR}" != 'x' && echo "${_STDERR}" 1>&2
    }

    for _COIN in "${_BACKUP[@]}"; do
        rm -rf "/${_COIN}.pacnew"
    done

    if test "${#_BACKUP[@]}" -gt 0; then
        rm -rf "/opt/xampp-backups/${_DATESTAMP}"
        install -dm755 "/opt/xampp-backups/${_DATESTAMP}"
        for _COIN in "${_BACKUP[@]}"; do
            if test -d "/${_COIN}"; then
                mv "/${_COIN}" "/opt/xampp-backups/${_DATESTAMP}/" || \
                    echo "ERROR: Could not create a backup of /${_COIN}" 1>&2
            fi
        done
    fi

    rm -rf '/opt/lampp/'
}

post_remove() {
    if [ -e "/usr/share/hunspell/pt_BR.dic" ]
    then
    echo ""
    echo -e "\e[34;1m==>\e[39;1m LEMBRE-SE: O backup da pasta htdocs foi feito em /opt/xampp-backups. \e[0m"
    echo ""
    else
    echo ""
    echo -e "\e[34;1m==>\e[39;1m REMEMBER: Your htdocs folder was backed up to /opt/xampp-backups. \e[0m"
    echo ""
    fi
}
