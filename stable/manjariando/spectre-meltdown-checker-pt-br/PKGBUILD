# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Simon Legner <Simon.Legner@gmail.com>

_pkgname=spectre-meltdown-checker
pkgname=${_pkgname}-pt-br
pkgver=0.44
pkgrel=1
pkgdesc="Spectre & Meltdown vulnerability/mitigation checker (CVE-2017-5753, CVE-2017-5715, CVE-2017-5754, CVE-2018-3640, CVE-2018-3639, CVE-2018-3615, CVE-2018-3620, CVE-2018-3646, CVE-2018-12126, CVE-2018-12130, CVE-2018-12127, CVE-2019-11091)"
arch=('any')
url="https://github.com/speed47/${_pkgname}"
license=('GPL3')
depends=('bash')
conflicts=("${_pkgname}")
provides=("${_pkgname}=${pkgver}")
source=("https://github.com/speed47/${_pkgname}/archive/v${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${_pkgname}"-{48,64,128}.png
        'translate-pt-br.patch')

sha256sums=('96765d765275476c36a146da123fa7e9eb310a84e84ae71b179c9ace3b6ab0c8'
            'e5fd7da09fc511de73ef6b271498b5fb4cc3b610a91394f243fe697ed74e1339'
            'ec8c806215f0d461101406e9ead9b1db30bfc4104b1c0298cffe1f9bc6bdf295'
            '755815c10bfd5ee2fa4572f01d981627460178b80dec559b4340910a7a7be8d7'
            'cd4d5bbad4b118e75e1627fd62f1e24b2fabec10addefaaceeda6052a2ded4f1'
            '895ac9e14adbe8313654f02e99a7b63a898e4d4fedcf74915fb44a617c30e3b9')

prepare() {
    cd "${srcdir}/${_pkgname}-${pkgver}"
    patch -p1 -i "${srcdir}/translate-pt-br.patch"
}

_spectre_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Security;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_spectre_desktop" | tee com.${_pkgname//-/_}.desktop
}

package() {
    cd "${_pkgname}-${pkgver}"
    install -Dm755 "${_pkgname}.sh" "${pkgdir}/usr/bin/${_pkgname}"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${_pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${_pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${_pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done
}
