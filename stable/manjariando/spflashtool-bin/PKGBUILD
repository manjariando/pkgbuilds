# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Omar Pakker <archlinux@opakker.nl>

_pkgname=spflashtool
pkgname=${_pkgname}-bin
pkgver=5.2104
pkgrel=1
pkgdesc="SP Flash Tool is an application to flash your MediaTek (MTK) SmartPhone."
arch=('i686' 'x86_64')
url="http://spflashtools.com/category/linux"
license=('GPL-3.0')
makedepends=('gendesk')
provides=("${_pkgname}")
conflicts=("${_pkgname}")
options=('!strip')
source=("http://spflashtools.com/wp-content/uploads/SP_Flash_Tool_v${pkgver}_Linux.zip"
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${_pkgname}/${_pkgname}"-{48,64,128}.png
        'https://www.gnu.org/licenses/gpl-3.0.txt'
        "60-${_pkgname}.rules")
sha256sums=('63100e5c157e56f6ed284c4c6b33f8a4b6e2c36f3fccba87db5f643a4b85c5d2'
            'cf232122531d1a20d4d51a0a4952d8313be26d10c707e26447262ef709c50678'
            '8bc360fac53e14bccbb4f6b02f7099dd5b9889aabda012b3cb579a1276383ad5'
            'f2f1b75f1e9e41d0b35ae8eb00d48f4d36e2d0904b6eeaeb92e55a0ff0640495'
            'fe0b9c1de77c687623bfc07733041d1387f755493cdf904e6afcb47f784d34c7'
            '3972dc9744f6499f0f9b2dbf76696f2ae7ad8af9b23dde66d6af86c9dfb36986'
            'a46a4fc667cf5d6114f3757dc8dbc6cfbc27229319d48f6d78c1e026b34210da')

# Workaround for source file download, which requires the 'Referer' header to be set
DLAGENTS=('http::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -e %u -o %o %u'
          "${DLAGENTS[@]}")

prepare() {
   # Create exec file. Required for the binary to find its own .so files
    {
        echo '#!/bin/sh'
        echo 'export LD_LIBRARY_PATH="/opt/'"${_pkgname}"'"'
        echo '/opt/'"${_pkgname}"'/flash_tool "${@}"'
    } > "${srcdir}/${_pkgname}"
}

_spflashtool_desktop="[Desktop Entry]
Version=1.0
Name=SP Flash Tool
Type=Application
Comment=SP Flash Tool is an application to flash your MediaTek (MTK) SmartPhone
Comment[pt_BR]=SP Flash Tool é um aplicativo para flash seu SmartPhone MediaTek (MTK)
Exec=/usr/bin/${_pkgname}
Icon=${_pkgname}
Categories=Development;"

build() {
    cd "${srcdir}"
    echo -e "$_spflashtool_desktop" | tee com.${_pkgname}.desktop
}

package() {
    depends=('qtwebkit')

    local folderName="SP_Flash_Tool_v${pkgver}_Linux"

    # Clean files we do not need
    rm "${srcdir}/${folderName}/flash_tool.sh"
    rm -r "${srcdir}/${folderName}/bin"
    rm -r "${srcdir}/${folderName}/lib"
    rm -r "${srcdir}/${folderName}/plugins"
    rm -r "${srcdir}/${folderName}/Driver"

    # Install remaining files
    install -Dm644 -t "${pkgdir}/opt/${_pkgname}/" "${srcdir}/${folderName}/"*

    # Mark the binary as executable and install the shell file created in prepare()
    chmod +x "${pkgdir}/opt/${_pkgname}/flash_tool"
    install -Dm755 "${srcdir}/${_pkgname}" "${pkgdir}/usr/bin/${_pkgname}"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${_pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/gpl-3.0.txt" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${_pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done

    # Udev rule
    install -Dm644 "${srcdir}/60-${_pkgname}.rules" "${pkgdir}/usr/lib/udev/rules.d/60-${_pkgname}.rules"

    # Link QT assistant so the help menu works
    install -dm755 "${pkgdir}/opt/${_pkgname}/bin"
    ln -sf "/usr/lib/qt4/bin/assistant" "${pkgdir}/opt/${_pkgname}/bin/assistant"
}
