# Maintainer: duffydack <duffydack73 {at] gmail {dot} com>

pkgname=zsh-auto-notify
pkgver=0.8.0
pkgrel=2
pkgdesc="Zsh plugin that sends out a notification when a long running task has completed"
arch=('any')
url="https://github.com/MichaelAquilina/zsh-auto-notify"
license=('GPL')
depends=('zsh')
install=${pkgname}.install
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/MichaelAquilina/${pkgname}/archive/${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/undistract-me/undistract-me"-{48,64,128}.png)
sha512sums=('a45372be1b38716c30a96fd4b50b85bd535595a6e183ba762f6a27e4249a3a3fa3f0f6337c2830885ff3bf0e1eff1a4bd6840898564fd0a100489d5d50f1d1e5'
            '94146486da871bdc217c1c255f927e727cc5601f9473f16dcb5233ea30a54625cce0fc481e618af5c4ccd4dd3443989cde3de044395a9579460e12b1bc3e5304'
            '843e1b30fe1e12d4d1567872116f3a9e7d01bc2e275af59ca8bff2a7d284402b3c3f52bcafff738d87fbf89e1dfd081e2c289d1e452a75b404c3d8aa7ec03a18'
            '7f033ebd891875750c970d86a93b4a06211252b27f072b71307c8401a6fcd3258f3b371b605d97424810176e3ecf098f91da3e6169d33129dea8357ddf008379'
            'd5c996e479212c1d74e3ea6cb6a11c9952085948b1a85cc001a8c59fa998763b88a96caf7e948fdb18c55decc1e8c1c893e4cb1f3b402f03de963421d62cdcba')

_notify_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_notify_desktop" | tee com.${pkgname//-/_}.desktop
}

package() {
    cd "${pkgname}-${pkgver}"
    install -vDm 644 auto-notify.plugin.zsh \
        -t "${pkgdir}/usr/share/zsh/plugins/${pkgname}/"
    # docs
    install -vDm 644 README.rst \
        -t "${pkgdir}/usr/share/doc/${pkgname}/"
    # license
    install -vDm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}/"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/undistract-me-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
