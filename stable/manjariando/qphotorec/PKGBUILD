# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Steven Honeyman <stevenhoneyman at gmail com>

pkgname=qphotorec
pkgver=7.2
pkgrel=4
pkgdesc="Checks and undeletes partitions. Includes PhotoRec signature based recovery tool."
arch=('i686' 'x86_64')
url="https://www.cgsecurity.org/wiki/PhotoRec"
license=('GPL')
makedepends=('qt5-tools')
conflicts=('testdisk' 'testdisk-wip')
provides=("testdisk=${pkgver}" 'photorec' 'fidentify')
source=("http://www.cgsecurity.org/testdisk-${pkgver}-WIP.tar.bz2"
        "org.freedesktop.policykit.qphotorec.policy"
        "https://metainfo.manjariando.com.br/${pkgname}/org.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{64,128}.png)
sha256sums=('f6a88ac5214389416517679f628ac48a77bc4b07309bf7fa86628ac0925c2ae4'
            'd9a349ca318979c117e580917fd03c69129d97350cb6510db621ca1b3c8e829e'
            'b7e75f5e8da179bfb64afdc39a741952aea2ea4b4a866f7bb3b0651262cfe541'
            '5603d4fd28c0e665a002c624bdad96f52014c103ded8bc95f9d5e0ecb14341c1'
            '255d1bf712671bc1e11eeb0ea3a156958a41b9c818a0970f96482b05c0e199cc')

prepare() {
    cd "${srcdir}/testdisk-${pkgver}-WIP"

    # Some fixes to make it Qt5 capable
    sed -i '/<QWidget>/d' src/*.h
    sed -i '/<QWidget>/d' src/*.cpp
    sed -i 's/Qt::escape\(.*\)text())/QString\1text()).toHtmlEscaped()/g' src/${pkgname}.cpp

    ## You might not need these, but I did for some reason
    test -f src/gnome/help-about.png || cp /usr/share/icons/gnome/48x48/actions/help-about.png src/gnome/
    test -f src/gnome/image-x-generic.png || cp /usr/share/icons/gnome/48x48/mimetypes/image-x-generic.png src/gnome/
}

build() {
    cd "${srcdir}/testdisk-${pkgver}-WIP"
    export QTGUI_LIBS="$(pkg-config Qt5Widgets --libs)"
    export QTGUI_CFLAGS="-fPIC $(pkg-config Qt5Widgets --cflags)"

    # add --disable-qt if you do not have qt5-base
    ./configure --prefix=/usr --enable-sudo
    make
}

package() {
    depends=('libjpeg' 'qt5-base')
    optdepends=('libewf: support EnCase files'
                'ntfs-3g: support NTFS partitions'
                'progsreiserfs: support ReiserFS partitions')

    cd "${srcdir}/testdisk-${pkgver}-WIP"
    make DESTDIR="${pkgdir}" install
    
    sed -i '/%F/ s/\/usr\/bin\/qphotorec/pkexec \/usr\/bin\/qphotorec/' "${pkgdir}/usr/share/applications/${pkgname}.desktop"
    install -Dm644 "${srcdir}/org.freedesktop.policykit.qphotorec.policy" "${pkgdir}/usr/share/polkit-1/actions/org.freedesktop.policykit.qphotorec.policy"
    
    # Appstream
    mv "${pkgdir}/usr/share/applications/${pkgname}.desktop" "${pkgdir}/usr/share/applications/org.${pkgname}.desktop"
    install -Dm644 "${srcdir}/org.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.${pkgname}.metainfo.xml"

    for i in 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
