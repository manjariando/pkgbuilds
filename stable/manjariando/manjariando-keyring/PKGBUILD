# Based on the file created for Arch Linux by:
# Pierre Schmitz <pierre@archlinux.de>

# Maintainer : Philip Müller <philm@manjaro.org>
# Maintainer : Guillaume Benoit <guillaume@manjaro.org>

pkgname=manjariando-keyring
pkgver=20200919
pkgrel=1
pkgdesc='Manjariando PGP keyring'
arch=('any')
url='https://gitlab.com/manjariando'
license=('GPL')
install="${pkgname}.install"
source=('Makefile'
        'manjariando.gpg'
        #'manjariando-revoked'
        'manjariando-trusted'
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/gpg"-{48,64,128}.png)
sha256sums=('d2d6d26e6715dcae57b15e10cbf4fd32599452b5f2072cf22e834ad63bb9e477'
            'aa203a7fc9cb8762df7e42c2d34424ecbb543b2b22b294bfa361e538327ee3b6'
            'bcbc606d0763dc7c39ec15b48fc12f7a098d5847e9fc8b5eccb057d68f273230'
            '56190bb59ad2f4fd7fbc5d49b26f623de55be1284a3db34b0cdd80032b5af879'
            '06344c82613c8c24598673fb14a566ff1b85d52a128eb41cb40e2ae3bff374cc'
            'd42855d3959b106cfc78f0c354fc452cd7f7d79a52503c0448da19dba0759ca1'
            'ff2c4188fcceedbe628cc857333f300d1b6d10f381b1c69a4a57a632c6334eb3')

pkgver() {
  _date=$(date +%F | sed s@-@@g)
  echo ${_date}
}

_manjariando_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_manjariando_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    depends=('repo-manjariando')

    cd "${srcdir}"
    make PREFIX=/usr DESTDIR=${pkgdir} install

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -d ${pkgdir}/usr/share/licenses/${pkgname}
    ln -s /usr/share/licenses/repo-manjariando/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/gpg-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
