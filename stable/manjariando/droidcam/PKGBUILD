# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: AwesomeHaircut <jesusbalbastro at gmail com>
# Contributor: Mateusz Gozdek <mgozdekof@gmail.com>
# Contributor: Rein Fernhout <public@reinfernhout.xyz>
# Past Contributor: James An <james@jamesan.ca>

pkgname=droidcam
pkgver=1.7.2
pkgrel=1
pkgdesc='A tool for using your android device as a wireless/usb webcam'
arch=('x86_64')
url="https://github.com/aramg/${pkgname}"
license=('GPL')
backup=("etc/modprobe.d/${pkgname}.conf")
makedepends=('gtk3' 'libappindicator-gtk3' 'ffmpeg' 'libusbmuxd')
options=('!strip')
source=("${pkgname}-${pkgver}.zip::${url}/archive/v${pkgver}.zip"
        "droidcam-modules.conf" "${pkgname}-modprobe.conf"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png
        'appindicator.patch')

sha512sums=('cbb1ba79f450885c2d1598acd2d176ed1b86fe08db71efe9ed5050e90262659e6b7e55a8d0b7decc90b968fbfaaea835e3985f356700dd96b92839fa780f03ae'
            'aa13c28c7618fa710604c633aa9178cc8a4086abd839915ae579b639405403cf4eb8b692b958032fe43aec07ba8afcb97b998fc9cbdb801004e3d17557eb687e'
            'ca62acc7075e1cd85d32ffd0031fb92cb20c2f9e025011af6b3c380d654268ebdd89d875ebc8d1570c93b78b4242077446e41e2dc033af104409fb5892dac112'
            '0548d8fd3ff57b24f371ab6dc61f0cf9a6450396cc7fcf08edd69360b8cd7d839286c22c9d1c094f57c310604f94c85982e34b451d4b0b752fcd93cff4919638'
            'a5f1fc7af095adcd715ad5b5bf109c6f2ccc03ba7897e4d520e55b1e7f8367967be87563f9c516ff0e1b0c76c79cfa07a964f61bc6f94ef38b62c6e788957fa5'
            '845054a565d5fd3921f7127e8d1948deaa028a63430c545647621cb5c5f7434add53cd93a89e2f926c081f905bffe8249cf14be5eedebb9126ac1c4fd37ceab2'
            '8974a76f9d25e114fea8c91e954be3364a1f1d392f034919fc3a63bec46811f8b2bba3fd1eb1ac0de51622c5435a398792e615db23c4c5d9d04fc936aeae8090'
            '3f8eb415ca381734c547b9d0d382d070c6eb2ac11547143a2dd10f8e30ea153037d94861987ee3284781426fad40b66f02a4ffe39c6129580a87cd457f9efcf1')

_droidcam_desktop="[Desktop Entry]
Encoding=UTF-8
Version=${pkgver}
Name=Droidcam
Type=Application
Exec=${pkgname}
Icon=${pkgname}
Categories=GNOME;AudioVideo;
Comment=A tool for using your Android phone as a wireless/usb webcam
Comment[pt_BR]=Uma ferramenta para usar seu telefone Android como uma webcam sem fio/usb"

prepare() {
    cd "${srcdir}/${pkgname}-${pkgver}"
    patch -p1 -i ${srcdir}/appindicator.patch
}

build() {
    cd "${srcdir}"
    echo -e "$_droidcam_desktop" | tee com.${pkgname}.desktop

    cd ${pkgname}-${pkgver}

    # All JPEG* parameters are needed to use shared version of libturbojpeg instead of
    # static one.
    #
    # Also libusbmuxd requires an override while linking.
    make JPEG_DIR="" JPEG_INCLUDE="" JPEG_LIB="" JPEG=$(pkg-config --libs --cflags libturbojpeg) USBMUXD=-lusbmuxd-2.0
}

package() {
    depends=('alsa-lib' "${pkgname}-modules" 'ffmpeg' 'libjpeg-turbo' 'libusbmuxd' 'speex')
    optdepends=('gtk3: use GUI version in addition to CLI interface'
                'libappindicator-gtk3: use GUI version in addition to CLI interface')

    pushd ${pkgname}-${pkgver}

    # Install droidcam program files
    mkdir -p ${pkgdir}/opt/${pkgname}
    install -Dm755 ${pkgname} "${pkgdir}/opt/${pkgname}/${pkgname}"
    install -Dm755 ${pkgname}-cli "${pkgdir}/opt/${pkgname}/${pkgname}-cli"
    install -Dm644 "${srcdir}/${pkgname}-modprobe.conf" "${pkgdir}/etc/modprobe.d/${pkgname}.conf"
    install -Dm644 "${srcdir}/${pkgname}-modules.conf" "${pkgdir}/etc/modules-load.d/${pkgname}.conf"
    install -Dm644 icon2.png "${pkgdir}/opt/${pkgname}-icon.png"

    # Symlink
    mkdir -p ${pkgdir}/usr/bin
    ln -s /opt/${pkgname}/${pkgname} "${pkgdir}/usr/bin/${pkgname}"
    ln -s /opt/${pkgname}/${pkgname}-cli "${pkgdir}/usr/bin/${pkgname}-cli"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}

