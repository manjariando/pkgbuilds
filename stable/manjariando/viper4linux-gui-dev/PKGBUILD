# Maintainer: Mark Wagie <mark dot wagie at tutanota dot com>

_pkgname=viper4linux-gui
pkgname=${_pkgname}-dev
pkgver=2.2.295
pkgrel=2
pkgdesc="Official UI for Viper4Linux"
arch=('x86_64')
url="https://github.com/Audio4Linux/Viper4Linux-GUI"
license=('GPL3')
depends=('viper4linux' 'gst-plugins-bad' 'qt5-svg' 'qt5-multimedia' 'mesa')
makedepends=('git' 'imagemagick')
optdepends=('libappindicator-gtk3: tray icon support')
provides=("${_pkgname}=${pkgver}")
conflicts=("${_pkgname}" "${_pkgname}-git")
install=${pkgname%-dev}.install
source=("${_pkgname}::git+https://github.com/Audio4Linux/Viper4Linux-GUI.git"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${_pkgname}.metainfo.xml")
sha256sums=('SKIP'
            '2d43ccfe23709dd731b2f4f57c94d47bd9286c4f1784a79f651bbfa219f82336')

pkgver() {
    cd "${srcdir}/${_pkgname}"
    _pkgver=$(git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g')
    printf '%s' "$( cut -f1-2 -d'.' <<< ${_pkgver} )"."$(git rev-list --count HEAD)"
}

_viper4linux_desktop="[Desktop Entry]
Name=Viper4Linux
GenericName=Equalizer
GenericName[pt_BR]=Equalizador
Comment=User Interface for Viper4Linux
Comment[pt_BR]=Interface de usuário para Viper4Linux
Keywords=equalizer
Categories=AudioVideo;Audio;
Exec=viper-gui
Icon=viper-gui
StartupNotify=false
Terminal=false
Type=Application"

build() {
    cd "${srcdir}"
    echo -e "$_viper4linux_desktop" | tee com.${_pkgname/-/_}.desktop

    cd "${srcdir}/${_pkgname}"
    qmake "CONFIG += FULLREQUIREMENTS" V4L_Frontend.pro
    make PREFIX=/usr
}

package() {
    cd "${srcdir}/${_pkgname}"
    install -Dm755 V4L_Frontend "$pkgdir/usr/bin/viper-gui"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${_pkgname/-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${_pkgname/-/_}.desktop"

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128 512; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${_pkgname}/viper.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/viper-gui.png"
    done
}
