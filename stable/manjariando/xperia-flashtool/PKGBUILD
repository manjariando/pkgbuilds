# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Muflone http://www.muflone.com/contacts/english/

pkgname=xperia-flashtool
_realname=flashtool
pkgver=0.9.29.0
pkgrel=1
pkgdesc="A S1 protocol flashing software for Sony Xperia phones"
arch=('i686' 'x86_64')
url="http://www.flashtool.net"
license=('GPL3')
depends=('libselinux' 'libsystemd' 'glib2' 'mono')
makedepends=('p7zip')
conflicts=("$pkgname")
provides=("$_realname")
install="${pkgname}.install"
source=("${_realname}-${pkgver}-linux.tar.xz"::"https://manjariando.gitlab.io/source-packages/${pkgname}/${_realname}-${pkgver}-linux.tar.xz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png
        'https://raw.githubusercontent.com/Androxyde/Flashtool/master/LICENSE'
        "org.freedesktop.policykit.${pkgname}.policy"
        "${pkgname}.sh")
sha256sums=('12b39bc3d5913a0ec2d184e893db9ff52a22312726dde07c2d1c1e950b69ec3e'
            '6829b201cdef1cd13a57a90dc10ddca6ad0e666c4825d03b232df360b899afb6'
            '52bc53f77c2642cc1b2b68790cf88c604c705966782993ad7b958eca8eb071b1'
            '8fc5f57eb51b177fa9944bc1e3280ceb375920bf022bc86ba81bb48a6c51855f'
            '8df0f250a1ff94e423428a8f6c206efe70d9acbf1d10bb45ffc7c48479f1001a'
            '3972dc9744f6499f0f9b2dbf76696f2ae7ad8af9b23dde66d6af86c9dfb36986'
            '4949c59138861e340473a0346997b4ecd30ea0d2962a2fd597360a6745ba5465'
            'b6b91cec623461e7b31bc3250045071350237962388ecd6df46bb437bc536803')
options=('!strip' '!upx')

_xperia_desktop="[Desktop Entry]
Version=1.0
Name=Xperia Flashtool
Type=Application
Comment=Firmware flasher for Xperia mobile devices
Comment[pt_BR]=Firmware Flasher para dispositivos móveis Xperia
Terminal=false
Exec=pkexec /usr/bin/xperia-flashtool
Icon=xperia-flashtool
Categories=Development;"

build() {
    cd "${srcdir}"
    echo -e "$_xperia_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    # Install all the program files
    install -m 755 -d "${pkgdir}/usr/lib/${pkgname}"
    cp -rt "${pkgdir}/usr/lib/${pkgname}" FlashTool/*

    # Install launcher scripts
    install -m 755 -d "${pkgdir}/usr/bin"
    install -m 755 "${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
    install -Dm644 "${srcdir}/org.freedesktop.policykit.${pkgname}.policy" \
        "${pkgdir}/usr/share/polkit-1/actions/org.freedesktop.policykit.${pkgname}.policy"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
