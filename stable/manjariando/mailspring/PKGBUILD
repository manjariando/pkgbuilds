# Maintainer: Eric S. Londres <ericlondres@protonmail.com>
# Contributor: Joakim Nylén <me@jnylen.nu>
# Contributor: Rashintha Maduneth <rashinthamaduneth@gmail.com>
# Contributor: Dhananjay Balan <mail@dbalan.in>
# Contributor: ahrs

pkgname=mailspring
pkgver=1.8.0
pkgrel=1
pkgdesc="A beautiful, fast and maintained fork of Nylas Mail by one of the original authors."
arch=('x86_64')
license=('custom: GPL3 and (C) 2016-2019 Foundry 376, LLC.')
url="https://getmailspring.com"
options=('!strip')

source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml")
source_x86_64=("https://github.com/Foundry376/Mailspring/releases/download/${pkgver}/mailspring-${pkgver}-amd64.deb")
sha256sums=('66bec93f0998ed74de84702467ed8fc3181962fef61ceeab486e49a0d320e5ed')
sha256sums_x86_64=('06dcd870737ceea1fbb371a206bb037e6bb2f6fdb176109e49286ef3e4fd4f71')

package() {
    depends=('libxss' 'libtool' 'c-ares' 'ctemplate' 'tidy' 'libxkbfile' 'libsecret' 'gtk3' 'nss' 'libglvnd')
    optdepends=('libappindicator-gtk3: for system tray support'
                'libgnome-keyring: keyrings'
                'gnome-keyring: keyrings')

    cd ${srcdir}

    tar -xvf data.tar.xz -C ${pkgdir} --exclude='./control'

    chmod -R go-w "${pkgdir}"/usr

    # Appstream
    mv "${pkgdir}/usr/share/applications/${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${pkgdir}/usr/share/doc/mailspring/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/Copyright"
    rm -rf "${pkgdir}/usr/share/appdata"
}
