# Maintainer: Troy Engel <troyengel+arch@gmail.com>
# Contributor: ugjka <ugis.germanis@gmail.com>
# Contributor: Bill Sun <billksun@gmail.com>
# Contributor: magnific0

pkgname=wondershaper
_pkgver=1.4.1
pkgver=1.4.1.r41
pkgrel=2
pkgdesc="Limit the bandwidth of one or more network adapters"
arch=('any')
url="https://github.com/magnific0/wondershaper"
license=('GPL2')
depends=('iproute')
makedepends=('git')
conflicts=("${pkgname}-git")
backup=('etc/systemd/wondershaper.conf')
install=${pkgname}.install
source=("${pkgname}"::"git://github.com/magnific0/wondershaper.git"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png
        "${pkgname}.timer")
sha256sums=('SKIP'
            '246a407c2db4e0b32680855cce58aea1efdc24d01365ad12bd1aa5af96ad7422'
            '6134ba55141421196df1ccb8e806f4b4eba7e1810460db92749e95c36f1e58e3'
            'c6b9d09fd0cd4d9c3f7f0e95886f4e3d5b43c3c7376d5e867f5ef2d0ccf4cb49'
            'badfca1942b64be87dae8084705ca4c4ee69ffb3764a5801f75a147ad0eb7c3a'
            'a840c2e0d49fc5aa2ca7079662d7ea75f1dd97ecfa370c5651dcd42c637fc97d')

_wondershaper_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Network;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_wondershaper_desktop" | tee com.${pkgname}.desktop
}

pkgver() {
  cd "${srcdir}/${pkgname}"
  printf "${_pkgver}.r%s" "$(git rev-list --count HEAD)"
}

package() {
  cd "${srcdir}/${pkgname}"
  install -Dm755 wondershaper "${pkgdir}/usr/bin/wondershaper"
  install -Dm644 COPYING "${pkgdir}/usr/share/license/${pkgname}/LICENSE"
  install -Dm644 wondershaper.service \
    "${pkgdir}/usr/lib/systemd/system/wondershaper.service"
  install -Dm644 "${srcdir}/wondershaper.timer" \
    "${pkgdir}/usr/lib/systemd/system/wondershaper.timer"
  install -Dm644 wondershaper.conf \
    "${pkgdir}/etc/systemd/wondershaper.conf"

  # Appstream
  install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
  install -Dm644 "${srcdir}/com.${pkgname}.desktop" \
    "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
