# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: FennecTECH <FennecTECH@gmail.com>
# Contributor: Wayne Hartmann <wayne@bitstorm.pw>
# Contributor: Martin Wimpress <code@flexion.org>
# Contributor: Johannes <maeulen@awp-shop.de>
# Contributor: Heiko Baums <heiko@baums-on-web.de>
# Contributor: wido <widomaker2k7@gmail.com>

pkgname=bitdefender
dist_build=7.7-1
pkgver=${dist_build/-/.}
pkgrel=47
pkgdesc="BitDefender's Personal UNIX Workstation Antivirus"
arch=('x86_64') #'i686'
url="https://www.bitdefender.com"
install=${pkgname}.install
license=('custom')
source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml")
#source_i686=("https://gitlab.com/manjariando/unsigned-packages/-/raw/master/bitdefender/i686/bitdefender-${pkgver}-45-i686.pkg.tar.xz"
#            ) #'cumulative-i686.zip::http://download.bitdefender.com/updates/update_av32bit/cumulative.zip')
source_x86_64=("https://gitlab.com/manjariando/unsigned-packages/-/raw/master/bitdefender/x86_64/bitdefender-${pkgver}-45-x86_64.pkg.tar.xz"
                ) #'cumulative-x86_64.zip::http://download.bitdefender.com/updates/update_av64bit/cumulative.zip')

sha256sums=('b8ef6fd38d5baeeb3fb5fd38ab87e52fa5948ef3b1f760fd81e8cae4ac848bd9')
sha256sums_x86_64=('aab45db7bd3aedb2c942f7079ddd4574a81608e9753ee83c09acbe07343220b9')
#sha256sums_i686=('983d20afce22bc95c9fc3eda3a6181ab9e084f370e2510731d9cead9d61c7d0d')

_bitdefender_desktop="[Desktop Entry]
Type=Application
Version=1.0
Name=BitDefender Antivirus Scanner for Unices
Name[pt_BR]=Verificador Antivírus BitDefender para Unices
GenericName=Antimalware Scanner
GenericName[pt_BR]=Scaneador Antimalware
GenericName[ro]=Scaner Antimalware
Comment=Antimalware Scanner
Comment[pt_BR]=Scaneador Antimalware
Comment[ro]=Scaner Antimalware
Icon=bitdefender
TryExec=bdgui
Exec=bdgui
Terminal=false
Categories=System;GTK;"

build() {
    cd "${srcdir}"
    echo -e "$_bitdefender_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=('gtk2' 'libstdc++5' 'atk' 'fontconfig' 'libxext' 'libxrender'
            'libxrandr' 'libxi' 'libxcursor' 'libxfixes' 'pango' 'glib2'
            'libxinerama' 'libsm' 'bitdefender-cumulative')

    cp -rPf usr "${pkgdir}"
    cp -rPf opt "${pkgdir}"
    cp -rPf etc "${pkgdir}"

    # latest deffinitions
    rm -rf ${pkgdir}/opt/BitDefender-scanner/var/lib/scan/Plugins
    #install -Dm644 ${srcdir}/Plugins/* ${pkgdir}/opt/BitDefender-scanner/var/lib/scan/Plugins

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    rm -rf "${pkgdir}/usr/share/applications/bdgui.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"

    for i in 16 22 32 48 64 128; do
        install -Dm644 "${pkgdir}/opt/BitDefender-scanner/share/doc/examples/icons/hicolor/${i}x${i}/apps/${pkgname}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
