# Maintainer: Xuanrui Qi <me@xuanruiqi.com>
# Contributor: Wayne Hartmann (DH4) <wayne@bitstorm.pw>

pkgname=ttf-wps-fonts
pkgver=1.0
pkgrel=7
pkgdesc="Symbol fonts required by wps-office."
arch=(any)
url="https://github.com/IamDH4/ttf-wps-fonts"
license=("custom")
makedepends=(git)
source=("${pkgname}::git+https://github.com/IamDH4/ttf-wps-fonts.git"
        "license.txt::https://aur.archlinux.org/cgit/aur.git/plain/license.txt?h=ttf-wps-fonts"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/fonts"-{48,64,128}.png)
sha512sums=('SKIP'
            'b8958766e99289206dc3efc2aa447b0810459b46dfba98dff4493fca73b328ad2c76065a1bf1456b133750f35e488b899e60d9931acc842af621703727d780c6'
            '51b8f776b1beca0e6ce783967ddff50a82187370b619cf3f8bdb315f87c1be1c6538d6f57bab4ea51a1c92b643de7506ceee461978370e561fa90e6f4d21fa7f'
            'c67f0b49bf91fd70f9e06719db86b8ef6fc07e097fa2758b7d2b50f4e199de73bba7559a20bce91226bdd84e75d06330ceee1982eccef02756e3b4fa6d697e63'
            'f7f6dd368d19fbcffe62abab9f83803799ac89f239dc629003bd08166f2447c19e3b1c268b4be13248b47969b2c08c1fe47b44767181d8bf1e8c730c99760e77'
            '956849cb11bd7f66d0a49ebd1fd8f83005e629b01f5706656880db45e38223a99e62886f39dc5c77434b5824df96f25944de9a2ef6a28ea90e4c13b18ddd5ca4')

_wps_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Office;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_wps_desktop" | tee com.${pkgname//-/_}.desktop
}

package() {
    install -d "${pkgdir}/usr/share/fonts/wps-fonts"
    install -m644 "${srcdir}/${pkgname}/"*.{ttf,TTF} "${pkgdir}/usr/share/fonts/wps-fonts/"
    install -Dm644 "${srcdir}/license.txt" "${pkgdir}/usr/share/licenses/${pkgname}/license.txt"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/fonts-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
