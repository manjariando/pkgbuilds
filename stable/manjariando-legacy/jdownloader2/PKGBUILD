# Maintainer: Holodoc

pkgname=jdownloader2
pkgver=latest
pkgrel=17
pkgdesc='Download manager, written in Java, for one-click hosting sites like Rapidshare and Megaupload. Uses its own updater.'
arch=('any')
conflicts=('jdownloader')
replaces=('jdownloader')
url='http://jdownloader.org'
license=('GPL')
makedepends=('imagemagick')
install='jdownloader.install'
source=("https://metainfo.manjariando.com.br/${pkgname}/org.jdownloader.metainfo.xml"
        'JDownloader'
        'JDownloaderHeadless'
        'JDownloaderHeadlessCtl'
        'functions.sh'
        'JDownloaderHeadlessCleanLogin'
        'jdownloader.xml'
        'jdownloader.desktop'
        'jd-containers.desktop'
        'jd-container256.png'
        'jdownloader256.png'
        'jdownloader.service' )
sha256sums=('7b0df87b7399ceed318053fe8de8801ec73e9d9927b150b9c812e017211f70ab'
            'cab5904f226028fdc9384f407ceca34b4305885176fad29b08a2e8b83653a345'
            'd555c78d8110e536aee67de765ee5134d872fbb48354050f7b2f14ff5499120a'
            'dca392fad29c70eff609ec25abaefd33343c8a6c98088e0719c6746759ed0aa5'
            '70f4a5cd95532c70ae20a357e655d01ac23fe92bb71feac45973e2b53d024cff'
            '1c9949bfeaf3595783eec9501e600cb8c4443e04f72d57c095560fb66dcd53d1'
            'c4301592694b3273ed44814debcc03bf1e4fc85882954f5c03e55508c53c4491'
            'b58fb23795900726dcb890ac9be293fd2fcbfeecee29f31e60cd1694ce3806d6'
            '92cfbe543ee1f9e094347dbd9c0c6a59bd52974145f00dbece8ed0da9a828bfa'
            '896eb67760bf0f3b2527b1f0cebee6cb0d16499af8961cb38bb5dca3e6d27d07'
            '6c7a28ec72c8627e9bf06a58d7f6bfed075632a6743e1c8087dc0fa065261504'
            '8d170fd301b37302a4f64cec759bdb5c879cb30c8b8e94120f3f985df1d31b7f')
package() {
    depends=('java-runtime' 'wget' 'bash' 'hicolor-icon-theme' 'unzip' 'shared-mime-info' 'desktop-file-utils' 'fontconfig' 'ttf-dejavu')
    optdepends=('phantomjs: needed for some remote capture solving')

    install -d -m755 "$pkgdir/opt/JDownloaderScripts"
    install -D -m755 "$srcdir/JDownloader" "$pkgdir/opt/JDownloaderScripts/JDownloader"
    install -D -m755 "$srcdir/JDownloaderHeadless" "$pkgdir/opt/JDownloaderScripts/JDownloaderHeadless"
    install -D -m755 "$srcdir/JDownloaderHeadlessCtl" "$pkgdir/opt/JDownloaderScripts/JDownloaderHeadlessCtl"
    install -D -m755 "$srcdir/functions.sh" "$pkgdir/opt/JDownloaderScripts/functions.sh"
    install -D -m755 "$srcdir/JDownloaderHeadlessCleanLogin" "$pkgdir/opt/JDownloaderScripts/JDownloaderHeadlessCleanLogin"

    install -D -m644 "$srcdir/jdownloader.xml" "$pkgdir/usr/share/mime/packages/jdownloader.xml"
    install -D -m644 "$srcdir/jdownloader.desktop" "$pkgdir/usr/share/applications/org.jdownloader.desktop"
    install -D -m644 "$srcdir/jd-containers.desktop" "$pkgdir/usr/share/applications/jd-containers.desktop"

    # Fix and install desktop icons
    for size in 16 22 24 32 48 64 128 256; do
        mkdir -p "${pkgdir}"/usr/share/icons/hicolor/${size}x${size}/{apps,mimetypes}
        convert "${srcdir}/jd-container256.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/mimetypes/jd-container.png"
        convert "${srcdir}/jdownloader256.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/jdownloader.png"
    done

    install -D -m644 "$srcdir/jdownloader.service" "$pkgdir/usr/lib/systemd/system/jdownloader.service"
    install -d -m775 "$pkgdir/opt/JDownloader"
    mkdir -p "$pkgdir/usr/bin"
  
    ln -s "/opt/JDownloaderScripts/JDownloader"  "${pkgdir}/usr/bin/JDownloader"
    ln -s "/opt/JDownloaderScripts/JDownloader" "${pkgdir}/usr/bin/jdownloader"
    ln -s "/opt/JDownloaderScripts/JDownloaderHeadless" "${pkgdir}/usr/bin/JDownloaderHeadless" 
    ln -s "/opt/JDownloaderScripts/JDownloaderHeadlessCtl" "${pkgdir}/usr/bin/JDownloaderHeadlessCtl"
    ln -s "/opt/JDownloaderScripts/JDownloaderHeadlessCleanLogin" "${pkgdir}/usr/bin/JDownloaderHeadlessCleanLogin"

    # Appstream
    install -D -m644 "${srcdir}/org.jdownloader.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.jdownloader.metainfo.xml"
}
