# Maintainer: Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: grimi <grimi at poczta dot fm>

pkgname=urbanterror
pkgver=4.3.4
pkgrel=1
arch=('i686' 'x86_64')
url="http://www.urbanterror.info"
license=('GPL2')
pkgdesc="A team-based tactical shooter based on the Quake 3 Engine"
depends=('sdl' 'openal' 'curl')
backup=('opt/urbanterror/q3ut4/server.cfg' 'opt/urbanterror/q3ut4/mapcycle.txt')
makedepends=('mesa')
options=('!strip')
source=("http://www.happyurtday.com/releases/4x/UrbanTerror${pkgver//.}_full.zip"
        "https://manjariando.gitlab.io/metainfo/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://manjariando.gitlab.io/metainfo/${pkgname}/${pkgname}"-{48,64,128}.png
        "${pkgname}.sh"
        "${pkgname}-server.sh")
sha512sums=('4debd6f998943cf9944ed891a3d1f3fa7d016d181d7af7cd5c28690bcd765928b4befc3f3705a4315bfa709acdd34160ed8f2d7437a54cfd10f6c48fea60af39'
            'c190554a8ea5a7e5d1171aae600eff733086b2eeaba01a3e15f00fa959890828ca7ea4995160cca34e02e313d9684ede7aa01667dc92d71bbb2278521050b26b'
            '4b1f9a803a107b8f8958e191460254b981e392c30e04bc5da2553bcb9b7cac724a247fa5e8074bdb556a02cbafe18fdf267cb2afa8c967250e615d009fa0b4a7'
            '642e0f81a299aa8ffe7d5bc1260e2bc2c80e6f463a63da21ecfdfac2a1847893a6d4076f5c629d7a03be660eafa355a8fb72b55fda9722ed47247eff94cbe454'
            'd0cb0607634546b1b767e033e3578413d0048cee1009d72ce46d95b4bd711c690ba46864d67313296a9975cbf63a594f29fe81f3031de42163978ed502364e63'
            'd296cee59c9a344b03485b2400278f05d4a4edd8f782f72b10857d4654d46ff1b6f181c51abb5a718d6b4a7227601d67fb2ba2b3fe7876bfec17b7c9827525a0'
            '6f444b1203b8c1a66c86a2112f3565059ba1135940cef37208ea05933290e1b3b9d8325c7fc86466fd8646338c8deaa168a22049104bda0631eab76a21a77e51')

_urbanterror_desktop="[Desktop Entry]
Version=1.0
Name=Urban Terror
Comment=A team-based tactical shooter based on the Quake 3 Engine
Comment[pt_BR]=Um jogo de tiro tático em equipe baseado no mecanismo Quake 3
Type=Application
Categories=Game;
Terminal=false
Exec=urbanterror
Icon=urbanterror"

build() {
  cd "${srcdir}"
  echo -e "$_urbanterror_desktop" | tee com.${pkgname}.desktop
}

package() {
    install -d "${pkgdir}/opt/${pkgname}"
    cd "${pkgdir}/opt/${pkgname}"

    # Copy binaries.
    if [ "${CARCH}" = "i686" ]; then
    install -m755 "${srcdir}/UrbanTerror43/Quake3-UrT.i386" ${pkgname}
    install -m755 "${srcdir}/UrbanTerror43/Quake3-UrT-Ded.i386" ${pkgname}-ded
    fi
    if [ "${CARCH}" = "x86_64" ]; then
    install -m755 "${srcdir}/UrbanTerror43/Quake3-UrT.x86_64" ${pkgname}
    install -m755 "${srcdir}/UrbanTerror43/Quake3-UrT-Ded.x86_64" ${pkgname}-ded
    fi

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    # Copy data
    cp -r "${srcdir}/UrbanTerror43/q3ut4" "${pkgdir}/opt/urbanterror/q3ut4"  
    install -Dm644 "${srcdir}/UrbanTerror43/q3ut4/readme43.txt" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    echo -e "\nseta cl_cURLLib \"/usr/lib/libcurl.so.4\"" >> "${pkgdir}/opt/urbanterror/q3ut4/autoexec.cfg"

    # Copy launch scripts.
    install -Dm755 "${srcdir}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
    install -Dm755 "${srcdir}/${pkgname}-server.sh" "${pkgdir}/usr/bin/${pkgname}-server"
}
