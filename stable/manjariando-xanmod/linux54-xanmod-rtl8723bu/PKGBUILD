# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Bob Fanger < bfanger(at)gmail >
# Filip <fila pruda com>, Det < nimetonmaili(at)gmail >

_linuxprefix=linux54-xanmod
_extramodules=extramodules-5.4-xanmod-MANJARO
pkgname=$_linuxprefix-rtl8723bu
_pkgname=rtl8723bu
_libname=8723bu
_pkgver=20210324
pkgver=20210324_5.4.108.xanmod1_1.1
pkgrel=1
pkgdesc="A kernel module for Realtek 8723bu network cards"
url="http://www.realtek.com.tw"
license=("GPL")
arch=('x86_64')
depends=("$_linuxprefix")
makedepends=("$_linuxprefix-headers")
builddepends=("$_linuxprefix-headers")
provides=("$_pkgname=$_pkgver")
groups=("$_linuxprefix-extramodules")
source=("${_pkgname}-${_pkgver}::https://github.com/lwfinger/rtl8723bu/archive/master.zip"
       	'blacklist-rtl8xxxu.conf')
sha256sums=('81b770e9fd4199bb82cd1f58d9c7f7b017380708217c87657ac0b2ea421e9c29'
            '7c726ad04083c8e620bc11c837e5f51d3e9e2a5c3e19c333b2968eb39f1ef07e')
install=rtl8723bu.install

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

build() {
    _kernver="$(cat /usr/lib/modules/$_extramodules/version || true)"
    _srcroot="$srcdir/$_pkgname-master"
    cd "${_srcroot}"
    # do not compile with CONCURRENT_MODE
    sed -i 's/EXTRA_CFLAGS += -DCONFIG_CONCURRENT_MODE/#EXTRA_CFLAGS += -DCONFIG_CONCURRENT_MODE/g' Makefile

    # avoid using the Makefile directly -- it doesn't understand
    # any kernel but the current.
    make clean
    make -C /usr/lib/modules/$_kernver/build M="$srcdir/$_pkgname-master" modules
}

package() {
    depends=("${_linuxprefix}")

    install -d m644 $pkgdir/etc/modprobe.d
    install -t "$pkgdir/etc/modprobe.d" "blacklist-rtl8xxxu.conf"

    install -D -m644 $srcdir/$_pkgname-master/$_libname.ko "$pkgdir/usr/lib/modules/$_extramodules/$_libname.ko"

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${install}"

    find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;
}
