# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

_linuxprefix=linux510-xanmod
_extramodules=extramodules-5.10-xanmod-MANJARO
pkgbase=$_linuxprefix-spl_zfs
pkgname=("$_linuxprefix-zfs")
_pkgver=2.0.4
pkgver=2.0.4_5.10.27.xanmod1_1.1
pkgrel=1
url="http://zfsonlinux.org/"
arch=('x86_64')
license=("CDDL")
depends=("$_linuxprefix" "kmod")
makedepends=("$_linuxprefix-headers")
groups=("$_linuxprefix-extramodules")
install=install
source=("https://github.com/zfsonlinux/zfs/releases/download/zfs-${_pkgver}/zfs-${_pkgver}.tar.gz")
sha256sums=('7d1344c5433b91823f02c2e40b33d181fa6faf286bea5591f4b1965f23d45f6c')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
  cd "${srcdir}/zfs-${_pkgver}"
  # patches here

}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
  cd "${srcdir}/zfs-${_pkgver}"
  ./autogen.sh
  sed -i "s|\$(uname -r)|${_kernver}|g" configure
  ./configure --prefix=/usr --sysconfdir=/etc --sbindir=/usr/bin --libdir=/usr/lib \
              --datadir=/usr/share --includedir=/usr/include --with-udevdir=/lib/udev \
              --libexecdir=/usr/lib/zfs-${_pkgver} --with-config=kernel \
              --with-linux=/usr/lib/modules/${_kernver}/build \
              --with-linux-obj=/usr/lib/modules/${_kernver}/build
  make
}

package(){
  pkgdesc='Kernel modules for the Zettabyte File System.'
  provides=("zfs=$_pkgver")
  depends+=("zfs-utils=${_pkgver}")
  replaces=('linux54-spl<=0.7.13')

  cd "${srcdir}/zfs-${_pkgver}"
  install -dm755 "$pkgdir/usr/lib/modules/$_extramodules"
  install -m644 module/*/*.ko "$pkgdir/usr/lib/modules/$_extramodules"
  find "$pkgdir" -name '*.ko' -exec gzip -9 {} +
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='$_extramodules'/" "$startdir/install"
}
