# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

_linuxprefix=linux511-xanmod
_extramodules=extramodules-5.11-xanmod-MANJARO
pkgname=$_linuxprefix-bbswitch
_pkgname=bbswitch
_pkgver=0.8
pkgver=0.8_5.11.11.xanmod1_1.1
pkgrel=1
pkgdesc="kernel module allowing to switch dedicated graphics card on Optimus laptops"
arch=('x86_64')
url="http://github.com/Bumblebee-Project/bbswitch"
license=('GPL')
depends=("$_linuxprefix")
makedepends=("$_linuxprefix-headers")
provides=("$_pkgname=$_pkgver")
groups=("$_linuxprefix-extramodules")
install=bbswitch.install
source=("$_pkgname-$_pkgver.tar.gz::https://github.com/Bumblebee-Project/bbswitch/archive/v${_pkgver}.tar.gz"
        'kernel57.patch')
sha256sums=('76cabd3f734fb4fe6ebfe3ec9814138d0d6f47d47238521ecbd6a986b60d1477'
            '3b8039f3cd32d2aa8ad0b2426f28faac218eacd134c1e39454c9feca9d612789')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    _kernel=${_ver}
    printf '%s' "${_pkgver}_${_kernel/-/_}"
}

prepare() {
  cd ${srcdir}/${_pkgname}-${_pkgver}
  patch -p1 -i ../kernel57.patch
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  cd ${srcdir}/${_pkgname}-${_pkgver}
  # KDIR is necessary even when cleaning
  make KDIR=/usr/lib/modules/${_kernver}/build
}

package() {
  depends=("$_linuxprefix")

  cd ${srcdir}/${_pkgname}-${_pkgver}
  install -D -m644 bbswitch.ko $pkgdir/usr/lib/modules/${_extramodules}/bbswitch.ko
  # gzip -9 modules
  find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;
  sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" $startdir/*.install
}
