# Maintainer : Erik Dubois <erik.dubois@gmail.com>
# Previous Maintainer: Maxime Gauduin <alucryd@archlinux.org>

_pkgname=numix-circle-icon-theme
pkgname=${_pkgname}-git
pkgver=20.09.19.62
pkgrel=1
pkgdesc='Circle icon theme from the Numix project'
arch=('any')
url='https://github.com/numixproject/numix-icon-theme-circle'
license=('GPL3')
depends=('numix-icon-theme')
makedepends=('git')
provides=("${_pkgname}" 'numix-circle-light-icon-theme')
conflicts=("${_pkgname}" 'numix-circle-light-icon-theme')
options=('!strip')
source=("${_pkgname}::git+https://github.com/numixproject/numix-icon-theme-circle.git"
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/cupertino-icons-theme/cupertino-icons-theme"-{48,64,128}.png)
sha256sums=('SKIP'
            '247a270c06683cae33c46c60404c23d3b889abf9465da975164e88f2063470bd'
            'c76bfd7d6255e177567049e5cfe87d0648e0c3660e89350ef5b6a26ec6917827'
            'f117997c9587d4e09b0b566f21ef8dffa31878bb563ac2d34830bf075551093b'
            '8964bec97145c10c8891c60a90c86fd937bf76543cd22fb2fdd4b43b97b63633')

pkgver() {
    cd ${_pkgname}

    _ver=$(git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g')
    printf '%s' "$( cut -f1-3 -d'.' <<< ${_ver} )." "$(git rev-list --count HEAD)"
}

_numix_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_numix_desktop" | tee com.numix_circle_icon_theme.desktop
}

package() {
    cd ${_pkgname}
    install -dm 755 "${pkgdir}"/usr/share/icons
    cp -dr --no-preserve='ownership' Numix-Circle{,-Light} "${pkgdir}"/usr/share/icons/

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.numix_circle_icon_theme.desktop" \
        "${pkgdir}/usr/share/applications/com.numix_circle_icon_theme.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/cupertino-icons-theme-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done
}
