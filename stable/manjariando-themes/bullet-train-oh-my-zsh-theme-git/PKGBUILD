# Maintainer: TheAifam5 <aifam96 at gmail dot com>

_pkgname=bullet-train-oh-my-zsh
pkgname=bullet-train-oh-my-zsh-theme-git
pkgver=r358
zshver=5.8
pkgrel=1
pkgdesc='An oh-my-zsh shell theme based on the Powerline Vim plugin'
arch=('any')
url='https://github.com/caiogondim/bullet-train-oh-my-zsh-theme'
license=('MIT')
depends=('zsh' 'oh-my-zsh' 'nodejs')
makedepends=('git' "zsh=${zshver}")
provides=("${pkgname%%-git}")
conflicts=("${pkgname%%-git}" 'oh-my-zsh-powerline-theme' 'oh-my-zsh-powerline-theme-git')
install=${pkgname%%-git}.install
source=('git+https://github.com/caiogondim/bullet-train-oh-my-zsh-theme.git'
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/oh-my-zsh-powerline-theme/oh-my-zsh-theme"-{48,64,128}.png)
sha256sums=('SKIP'
            '5f11ce884430a3609703c36684e11960a85bb382a8ef64006aa89cbc205738e6'
            'e59d45e84180304b483f4f70dccb5577fcd35dd0b96a336eeaf8d720e9ef8a12'
            '610fa5c143c472fdd7e35e67f98fe01a66bd9d85266754bae9ff148790c5fecb'
            '17d3c28a3a73711d734fcccb5a43e5d15b98ca922c21cd7164739c718506c9f6')

pkgver() {
    cd "${pkgname%%-git}"
    printf "r%s" "$(git rev-list --count HEAD)"
}

_zsh_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_zsh_desktop" | tee com.bullet_train_oh_my_zsh.desktop
}

package() {
    cd "${pkgname%%-git}"

    # We don't need anything related to git in the package
    rm -rf .git*

    # License is inside of README
    install -D -m644 README.md "${pkgdir}/usr/share/licenses/${pkgname%%-git}/LICENSE"

    # Install empty documentation
    install -D -m644 README.md "${pkgdir}/usr/share/doc/${pkgname%%-git}/README.md"

    # Install the theme
    install -D -m644 bullet-train.zsh-theme "${pkgdir}/usr/share/oh-my-zsh/themes/bullet-train.zsh-theme"
    
    sed -i "s:BULLETTRAIN_last_exec_duration -gt:BULLETTRAIN_last_exec_duration:" "${pkgdir}/usr/share/oh-my-zsh/themes/bullet-train.zsh-theme"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.bullet_train_oh_my_zsh.desktop" \
      "${pkgdir}/usr/share/applications/com.bullet_train_oh_my_zsh.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/oh-my-zsh-theme-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done

    # fix .install file
    sed -i "s:ZSH=.*:ZSH=${zshver}:" "${startdir}/${pkgname%%-git}.install"
}
