# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=photogimp-snap
gimpver=2.10
snapver=347
_pkgver=1.0
pkgver=2020.${_pkgver}.${snapver}
pkgrel=1
arch=('x86_64')
license=('GPL-3.0' 'MIT')
url="https://github.com/Diolinux/PhotoGIMP"
pkgdesc="Photoshop theme for Gimp Snap"
install=${pkgname}.install

__commit="edbde0689965f2d9a2c29b6df02ade4440d90e97"

source=("photogimp-2020.${_pkgver}.zip::https://github.com/Diolinux/PhotoGIMP/archive/${__commit}.zip"
        "https://metainfo.manjariando.com.br/photogimp/com.${pkgname}.metainfo.xml"
        'https://www.gnu.org/licenses/gpl-3.0.txt'
        "${pkgname}")
sha256sums=('541dc5baf60bcd2b3631f8c7a319e5c0c478d3090c3bdb2e6ce221cd88a19235'
            'c1fc83b367c41dda8cd33d6e3d6d0ed6ae06da45ec6ad3ff867f28cbff9af006'
            '3972dc9744f6499f0f9b2dbf76696f2ae7ad8af9b23dde66d6af86c9dfb36986'
            '24029711a02b6de2d7c5fe04f5a1bee29aa56b86dc8050f1daa8b1a3272123b9')

_check_theme="PhotoGIMP
Version ${pkgver}"

_photogimp_desktop="[Desktop Entry]
Version=${pkgver}
Terminal=false
Type=Application
Name=PhotoGIMP Snap
Exec=env BAMF_DESKTOP_FILE_HINT=/var/lib/snapd/desktop/applications/gimp_gimp.desktop /var/lib/snapd/snap/bin/gimp %U
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_check_theme" | tee check.theme
    echo -e "$_photogimp_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    cd "${srcdir}/PhotoGIMP-${__commit}/.var/app/org.gimp.GIMP/config/GIMP/${gimpver}"

    install -Dm755 "${srcdir}/${pkgname}" "${pkgdir}/usr/bin/${pkgname}"
    mkdir -p "${pkgdir}/opt/${pkgname}/config/"{brushes,filters,internal-data,splashes,tool-options}
    cp -r {*rc,tags.xml,*-history} "${pkgdir}/opt/${pkgname}/config"
    cp -r {brushes,filters,fonts,internal-data,plug-ins,splashes,tool-options} "${pkgdir}/opt/${pkgname}/config"
    mv "${pkgdir}/opt/${pkgname}/config/brushes/Sem título.gbr" "${pkgdir}/opt/${pkgname}/config/brushes/Sem titulo.gbr"
    install -Dm644 "${srcdir}/PhotoGIMP-${__commit}/.local/share/applications/org.gimp.GIMP.desktop" "${pkgdir}/opt/${pkgname}/org.gimp.GIMP.desktop"
    sed -i "s:Icon=.*:Icon=photogimp-snap:" "${pkgdir}/opt/${pkgname}/org.gimp.GIMP.desktop"
    sed -i "s:Name=.*:Name=PhotoGIMP Snap:" "${pkgdir}/opt/${pkgname}/org.gimp.GIMP.desktop"
    sed -i "s:GenericName=.*:GenericName=Image Editor:" "${pkgdir}/opt/${pkgname}/org.gimp.GIMP.desktop"
    sed -i "s:Exec=.*:Exec=env BAMF_DESKTOP_FILE_HINT=\/var\/lib\/snapd\/desktop\/applications\/gimp_gimp.desktop \/var\/lib\/snapd\/snap\/bin\/gimp %U:" "${pkgdir}/opt/${pkgname}/org.gimp.GIMP.desktop"

    sed -i "s:GIMPVER=.*:GIMPVER=${gimpver}:" "${pkgdir}/usr/bin/${pkgname}" "${startdir}/${pkgname}.install"
    sed -i "s:SNAPVER=.*:SNAPVER=${snapver}:" "${pkgdir}/usr/bin/${pkgname}" "${startdir}/${pkgname}.install"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/gpl-3.0.txt" "${pkgdir}/usr/share/licenses/${pkgname}/GPL3"
    install -Dm644 "${srcdir}/check.theme" "${pkgdir}/opt/${pkgname}/config"

    for i in 16 32 48 64 128 256 512; do
        install -Dm644 "${srcdir}/PhotoGIMP-${__commit}/.local/share/icons/hicolor/${i}x${i}/apps/photogimp.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
