# Maintainer: tioguda <guda.flavio@gmail.com>

pkgbase=manjariando
pkgname=bootsplash-theme-${pkgbase}
pkgver=20210222
pkgrel=1
url="https://manjariando.com.br/2019/06/15/bootsplash-do-blog"
arch=('any')
license=('GPL')
makedepends=('imagemagick')
install=bootsplash-theme.install
options=('!libtool' '!emptydirs')

source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/boot"-{48,64,128}.png
        'bootsplash-packer'
        'bootsplash-packer.rst'
        'bootsplash-manjariando.sh'
        'bootsplash-manjariando.initcpio_install'
        'spinner.gif'
        'logo.png')

sha256sums=('b16aa53190b2f18ad9f210fe245a58048210993179e696efc221391792175e9f'
            'db12910980a9669036b4bfde58638b5349f011a5dbf808be1d617a5cff13d058'
            'df4c3d60e860f3fb129cb372380864060cd197d49bfe05f9cf8871001fbf36f2'
            '1857532b9c74ef8e24ceb79050bea2954bc377a632eaabb83c83c375df616a29'
            '51559d3ccfb448b03fa6439faf5869dbd0c2fbda1b5d5bf5d4ba70e60937472a'
            '35596e3f9c62d5f50730992befb940bfa75a24a2da15b558364300b52b64aa87'
            'c8f16087c51a3675c4809a0bb424c79caf04dabec858bd0a2757c78b2868f055'
            '04bc16db481a1a8375e8826aa84f98e1ae51a4a5a3985b76327ed3f32dbba553'
            'd5ad514cf95f08f551829095b664c7add918ad3fc6a8195ff8c54c55605d7ff5'
            'c40e1ba2d2cd24be7539c2b3ecc41e4d056dcdd50dc254dbbe1dc716a0f3bede')

pkgver() {
  _date=$(date +%F | sed s@-@@g)
  echo ${_date}
}

_bootsplash_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_bootsplash_desktop" | tee com.${pkgname//-/_}.desktop
    sh ./bootsplash-manjariando.sh
}

package() {
    pkgdesc="Bootsplash Theme 'Manjariando'"
    depends=('bootsplash-manager')

    cd "${srcdir}"

    install -Dm644 "${srcdir}/bootsplash-manjariando" "${pkgdir}/usr/lib/firmware/bootsplash-themes/manjariando/bootsplash"
    install -Dm644 "${srcdir}/bootsplash-manjariando.initcpio_install" "${pkgdir}/usr/lib/initcpio/install/bootsplash-manjariando"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -d ${pkgdir}/usr/share/licenses/${pkgname}
    ln -s /usr/share/licenses/repo-manjariando/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/boot-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:THEME=.*:THEME=${pkgbase}:" "${startdir}/bootsplash-theme.install"
}
