# Maintainer: tioguda <guda.flavio@gmail.com>

_pkgbase=helppack
pkgbase=libreoffice-dev-${_pkgbase}
_pkgnamefmt=LibreOffice
_LOver=7.1.2.2
pkgver=$(cut -f1-4 -d'.' <<< ${_LOver})
pkgrel=1
arch=('x86_64')
license=('LGPL3')
url="https://www.libreoffice.org"
pkgdesc="Help pack for LibreOffice development branch"

_languages=(
  'ar     "Arabic" "ar"'
  'ast    "Asturian" "ast"'
  'bg     "Bulgarian" "bg"'
  'bs     "Bosnian" "bs"'
  'ca     "Catalan" "ca"'
  'cs     "Czech" "cs"'
  'da     "Danish" "da"'
  'de     "German" "de"'
  'el     "Greek" "el"'
  'en-GB  "English (British)" "en-gb"'
  'en-ZA  "English (South African)" "en-ZA"'
  'eo     "Esperanto" "eo"'
  'es     "Spanish" "es"'
  'et     "Estonian" "et"'
  'eu     "Basque" "eu"'
  'fi     "Finnish" "fi"'
  'fr     "French" "fr"'
  'gl     "Galician" "gl"'
  'he     "Hebrew" "he"'
  'hr     "Croatian" "hr"'
  'hu     "Hungarian" "hu"'
  'id     "Indonesian" "id"'
  'is     "Icelandic" "is"'
  'it     "Italian" "it"'
  'ja     "Japanese" "ja"'
  'km     "Khmer" "km"'
  'ko     "Korean" "ko"'
  'mk     "Macedonian" "mk"'
  'nl     "Dutch" "nl"'
  'pl     "Polish" "pl"'
  'pt-BR  "Portuguese (Brazilian)" "pt-br"'
  'pt     "Portuguese (Portugal)" "pt"'
  'ro     "Romanian" "ro"'
  'ru     "Russian" "ru"'
  'si     "Sinhala" "si"'
  'sk     "Slovak" "sk"'
  'sl     "Slovenian" "sl"' 
  'sq     "Albanian" "sq"'
  'ta     "Tamil" "ta"'
  'tr     "Turkish" "tr"'
  'uk     "Ukrainian" "uk"'
  'vi     "Vietnamese" "vi"'
)

pkgname=()
source=()
_url=https://dev-builds.libreoffice.org/pre-releases/rpm

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=libreoffice-dev-help-${_locale,,}
  
  pkgname+=($_pkgname)
  source_x86_64+=("$_url/x86_64/${_pkgnamefmt}_${_LOver}_Linux_x86-64_rpm_${_pkgbase}_${_locale}.tar.gz")

sha256sums_x86_64=('88221f6039267c01e8c195d2b221499c0bca13ea17ea21ce1d1ee5be76c8f142'
                   '665d8a2f1c4d02dff828b7fd63aac454e8ce9edd293d1382b7b5f02bfac625bb'
                   'c827aae7731612dab07a9b1816873330a2fa16ce970e1aaa4d5355adc4ea54e3'
                   'c7294ff21a12fcdd04f79ba1d238e55c6dd17cba078639cb64073d125ce51058'
                   '520bbbbb13b83baa30441cfe8a2d26c4a8fee445fa3f2609f8e7e7fe05e0177c'
                   '506f9159e45a769631c0d4d04d2ff5d11eba9c822d795bc59884d251407b291d'
                   '3dc92999039dabd723a1b42832a9ba06d1336e5439ecaf83785f5e0b60bb302b'
                   '38c6f2168c929b9f3dd867ac81c42ffcd1f58cd4d24609d53df40144dcea4932'
                   '5cbed6f2b5cebf99ccef86d23edae9ef96c7733d070ed5a3635c74ce7d3ec0c9'
                   '3a7f7bcef76b0955dff52b65469feb3e0cb2e012c7748bf599c81997b30d63fc'
                   'd70f0865ea6d59b3e970f812ab311898c17252b2121110ef53e3275fb9a40ee9'
                   '3c874d8aec8da08a008919177075d3e2395d7743df309eed8da2075896a16472'
                   '77e0b8bce44dcde249a7492484dbad2535095b6b3716173030437513bbf7a596'
                   '20c95ac888dbaffca543d0f17919a5d84eeaaac516c1ef7d95e146f7926735d5'
                   'c5c0eb35ee294d7c8950161e82f5a824b23db19fad5e33189f78c0144df3c5ea'
                   '4159e6948815a9de0f88b29d2bae44d45ab33c01566d51ef0e3e00f607d464d9'
                   '9293062039cc4036d21928c3bfff6fa1ebba2020b4266fbc078c8aad467a4387'
                   '76908a38fdf807816dfe4714243b1893817999d52e66fc58be617b3af8b9f91e'
                   'de839b24df1a074ae47f5bd52f8992838b73cad652342b3b3153559196496d27'
                   'b0a85a1328d9a55b2b63850f10784b892ffba94a9f60f72dd13f5b00125e2bb4'
                   '020b6873e1df0add3a0bc725af71b42778839ac054e17ca47c3772e458ed2c3d'
                   'e4f3465db19dadeeb3ca9fea82a8bb4bcd6ded7c60a03198d557fea1086aca76'
                   '130c67eaf30f3ab9ef5f92385ff29b1b0e1323464959a14a72f4de38e0f3199f'
                   'cb204c19b9f86c28791ece850ee98ffcc47fa840a25934a9326aeabefe70639f'
                   '50cba7e9b2013fc75522e8df060c5c84065abf15a8709b32b6203df764f86b72'
                   'b72691b64aabed1a9bcd24332c0fb6eed026a70ed3414e77a3033cc22b8ee428'
                   'b59f111426daf212aca70f154f367477a4ff9b15b7eb3e9355473ce6794da617'
                   'd2cfb0c4de0c51d8312a0a4381936df97f320a2268a81a1a7921fcc7a7a752d9'
                   '44203d039a6ebc3100c6db1466e76d720f0ea55dda142efa823e4c6bb9b0b161'
                   'bd7075b2764626a4dbcfef15bb8ffb061ebd22421515bb04c9858ec9552aa126'
                   'daa314e43eeb46ca69239b26f870b1ef6379d753542d9ae169db2e38d6d53bf8'
                   'fe3cef82d7d73e12ccd6531bcefd8d49f1e0da99fb7b41110e21f1ce5f7b140c'
                   '56c3a8c08f3c1fa1eb077921fa6a8ad7320199cfacd14f9b3b50e8dbc3a30683'
                   '3fc0c40148a27be439f1d651719c052a8525dd4ec7cdb63ce3e5bb620c94cabb'
                   'e57de86c3b4c241005fffce9dd1c9b9af169f7a41e691f63eca0de8331d807dc'
                   'cf8276d1cda44e5e758a5030d2f251aa5a83e4440030fa062db9f73d340978eb'
                   'c60c9cd60a1ec1a0a55b01a64f76a40d5120872f1ffca8f2e4d58ba0003e1df6'
                   '014add07e21e8c6fb7ab690532ffe91a4037f60f2bc0ea3df40d8e4c06ba24af'
                   'eb25cf5ceef67fc1e0a60b992bcd5eb625e93b264afdd8118bd286cca972e926'
                   '8be643adb97f0355ce13a228e67b63ee4379e9b0f016be0f915d47bb4352ed51'
                   '01111e8505a9f08f9c62c0048830a13c6ea880ef06f762ec7db5655560cf0ebb'
                   'c7d46083da845c6c4f8d986ad2117e9df25766546898ffd5eaa4ff1c16c7f125')
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

_package() {
    pkgdesc="$2 help pack for LibreOffice development branch"
    __locale=$1
    depends=("libreoffice-dev>=${pkgver}")
    provides=("libreoffice-dev-help-$3=${pkgver}")
    replaces=("libreoffice-dev-help-$3-bin")
    
    find "${srcdir}/${_pkgnamefmt}_${_LOver}"*${__locale}/RPMS/*rpm -exec bsdtar -x -f '{}' -C "${pkgdir}" \;
}
