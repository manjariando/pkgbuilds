# Maintainer: Maxime Gauduin <alucryd@archlinux.org>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Tom Newsom <Jeepster@gmx.co.uk>
# Contributor: Paul Mattal <paul@archlinux.org>

pkgname=ffmpeg2.8
pkgver=2.8.17
pkgrel=2.2
pkgdesc='Complete solution to record, convert and stream audio and video'
arch=('i686' 'x86_64')
url='https://ffmpeg.org'
license=('GPL3')
depends=('alsa-lib' 'bzip2' 'fontconfig' 'fribidi' 'glibc' 'gmp' 'gnutls' 'gsm'
        'jack' 'lame' 'libmodplug' 'libpulse' 'libsoxr' 'libssh' 'libtheora'
        'libvdpau' 'libwebp' 'libx11' 'libxcb' 'libxext' 'libxv'
        'opencore-amr' 'openjpeg' 'opus' 'schroedinger' 'sdl' 'speex'
        'v4l-utils' 'xz' 'zlib'
        'libass.so' 'libbluray.so' 'dcadec' 'libfreetype.so' 'libva.so'
        'libvidstab.so' 'libvorbisenc.so' 'libvorbis.so' 'libvpx.so'
        'libx264.so' 'libx265.so' 'libxvidcore.so')
makedepends=('ladspa' 'yasm')
optdepends=('ladspa: LADSPA filters')
provides=('libavcodec.so' 'libavdevice.so' 'libavfilter.so' 'libavformat.so'
            'libavresample.so' 'libavutil.so' 'libpostproc.so' 'libswresample.so'
            'libswscale.so')
source=("https://ffmpeg.org/releases/ffmpeg-${pkgver}.tar.xz"{,.asc})
validpgpkeys=('FCF986EA15E6E293A5644F10B4322F04D67658D8') # ffmpeg-devel
sha256sums=('d0734fec613fe12bee0b5a84f917779b854c1ede7882793f618490e6bbf0c148'
            'SKIP')

build() {
    cd ffmpeg-${pkgver}

    export CFLAGS="-fPIC"
    export CXXFLAGS="$CFLAGS"

    ./configure \
        --prefix='/usr' \
        --incdir='/usr/include/ffmpeg2.8' \
        --libdir='/usr/lib/ffmpeg2.8' \
        --shlibdir='/usr/lib/ffmpeg2.8' \
        --disable-debug \
        --disable-static \
        --disable-stripping \
        --enable-avisynth \
        --enable-avresample \
        --enable-fontconfig \
        --enable-gnutls \
        --enable-gpl \
        --enable-ladspa \
        --enable-libass \
        --enable-libbluray \
        --enable-libdcadec \
        --enable-libfreetype \
        --enable-libfribidi \
        --enable-libgsm \
        --enable-libmodplug \
        --enable-libmp3lame \
        --enable-libopencore_amrnb \
        --enable-libopencore_amrwb \
        --enable-libopenjpeg \
        --enable-libopus \
        --enable-libpulse \
        --enable-libschroedinger \
        --enable-libsoxr \
        --enable-libspeex \
        --enable-libssh \
        --enable-libtheora \
        --enable-libv4l2 \
        --enable-libvidstab \
        --enable-libvorbis \
        --enable-libvpx \
        --enable-libwebp \
        --enable-libx264 \
        --enable-libx265 \
        --enable-libxvid \
        --enable-shared \
        --enable-version3 \
        --enable-x11grab
    make
}

package() {
    cd ffmpeg-${pkgver}

    make DESTDIR="${pkgdir}" install
    rm -rf "${pkgdir}"/usr/share

    find "${pkgdir}"/usr/bin -type f -exec mv {} {}2.8 \;

    install -dm 755 "${pkgdir}"/etc/ld.so.conf.d
    echo -e '/usr/lib/\n/usr/lib/ffmpeg2.8/' > "${pkgdir}"/etc/ld.so.conf.d/50-ffmpeg2.8.conf
}
